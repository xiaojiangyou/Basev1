<?php 
return array(
		// 提示
		'POST_AGAIN'=>'您的操作太快，请休息一会！',
		
		// 头部公共部分
		'LOGO'=>'<img src="'.__ROOT__.'/Public/Images/Home/blue/logo.png"/>',
		'LOGO_NAME'=>'',
		'SUB_LOGO_NAME'=>'',
		'FRIEND_LINK'=>'友情链接',
		
		// 网站首页
		'HTML_TITLE_INDEX'=>'首页',
		'INDEX_INTRODUCTION_TITLE'=>'本站简介',
		'INDEX_INTRODUCTION'=>'',
		
		// 内容页面
		'CLICK_NUM'=>'点击数：',
		'POST_DATE'=>'发布时间：',
		'PRE_POST'=>'上一篇：',
		'NEXT_POST'=>'下一篇：',
		
		/*搜索页*/
		'HTML_TITLE_SEARCH'=>'搜索',
		//'LATEST_SEARCH'=>'刚刚搜索过',
		'SEARCH_TITLE'=>'的搜索结果',
		'SEARCH_POST_TITLE'=>'文章标题',
		'SEARCH_POST_MODULE'=>'所属版块',
		'SEARCH_POST_DATE'=>'发布日期'
);
?> 