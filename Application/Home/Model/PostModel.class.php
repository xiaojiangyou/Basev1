<?php
namespace Home\Model;
use Think\Model\RelationModel;

use Think\Model;

define('DEFAULT_COUNTS',20);

class PostModel extends RelationModel {
	protected $_link = array(
			'Module'=> array(
					'mapping_type' =>  self::BELONGS_TO,
					'foreign_key'  =>  'mid'
			)
	);
// 	/**
// 	 * 获得相应的帖子
// 	 * @param int $mid 版块的id，默认0全部取出
// 	 * @param int $tid 帖子的id，设置则取出相应的帖子，默认为0全部取出
// 	 * @param int $counts 取出的数目，默认为20，为0表示全部取出
// 	 * @param string $surround 是否取出周围帖子
// 	 * @param string $order 排序方式
// 	 * @param int $comments 最少评论数
// 	 * @param int $views 最少访问数
// 	 */
// 	function get_posts($mid=0,$tid=0,$counts=DEFAULT_COUNTS,$surround=false,$order='dateline desc',$comments=0,$views=0){
// 		$where=array();
// 		if(!empty($mid)){
// 			$where['mid']=$mid;
// 		}
// 		if($comments>0){
// 			$where['comments']=array('ge',$comments);
// 		}
// 		if($views>0){
// 			$where['views']=array('ge',$views);
// 		}
// 		$fields='id,mid,title,dateline,comments,views';//取出的字段
// 		if($tid){
// 			$fields.=',content';
// 			$where['id']=$tid;
// 			$result=$this->where($where)->field($fields)->find();
// 			if($result){
// 				$result['date']=time2date($result['dateline']);
// 				$result['content']=htmlspecialchars_decode($result['content']);
// 			}
// 			if($surround){
// 				$where['id']=array('lt',$tid);
// 				$this->order('dateline desc');
// 				$next=$this->where($where)->field($fields)->find();
// 				$where['id']=array('gt',$tid);
// 				$this->order('id asc');
// 				$pre=$this->where($where)->field($fields)->find();
// 				return array('data'=>$result,'pre'=>$pre,'next'=>$next);
// 			}
// 			return array('data'=>$result);
// 		}
// 		else{
// 			$total=$this->where($where)->count();
// 			if($total<1) return array();
// 			$counts=$counts==0?$total:$counts;
// 			$this->where($where)->order($order);
// 			$page = new \Think\Page($total,$counts);
// 			if($mid!=0){
// 				//保证链接地址正确
// 				$page->setConfig('url', __APP__.'/mod/'.$mid);
// 			}
// 			//修改配置信息
// 			default_pager($page);
// 			$show = $page->show();
// 			$result=$this->limit($page->firstRow.','.$page->listRows)->field($fields)->select();
// 			foreach ($result as $i=>$r){
// 				$result[$i]['date']=time2date($r['dateline']);
// 			}
// 			return array('page'=>$show,'data'=>$result);
// 		}
// 	}
	/**
	 * 获得相应的帖子
	 * @param array $options
	 * @param int $mid 版块的id，默认0全部取出
	 * @param int $tid 帖子的id，设置则取出相应的帖子，默认为0全部取出
	 * @param int $counts 取出的数目，默认为20，为0表示全部取出
	 * @param string $surround 是否取出周围帖子
	 * @param string $order 排序方式
	 * @param int $comments 最少评论数
	 * @param int $views 最少访问数
	 * @return array
	 */
	function get_posts($options){
		$where=array();
		if(!empty($options['mid'])){
			if(is_array($options['mid'])){
				$where['mid']=array('in',$options['mid']);
			}else{
				$where['mid']=$options['mid'];
			}
		}
		if(isset($options['img'])&&$options['img']){
			array_push($where, array('EXP',"not null"));
		}
		if(isset($options['comments'])&&$options['comments']>0){
			$where['comments']=array('ge',$options['comments']);
		}
		if(isset($options['views'])&&$options['views']>0){
			$where['views']=array('ge',$options['views']);
		}
		$fields='id,mid,title,dateline,top,comments,views';//取出的字段
		if(!empty($options['tid'])){
			$fields.=',content';
			$where['id']=$options['tid'];
			$result=$this->where($where)->field($fields)->find();
			if($result){
				$result['date']=time2date($result['dateline']);
				$result['content']=htmlspecialchars_decode($result['content']);
			}
			if(isset($options['surround'])&&$options['surround']){
				$where['id']=array('lt',$options['tid']);
				$this->order('dateline desc');
				$next=$this->where($where)->field($fields)->find();
				$where['id']=array('gt',$options['tid']);
				$this->order('id asc');
				$pre=$this->where($where)->field($fields)->find();
				return array('data'=>$result,'pre'=>$pre,'next'=>$next);
			}
			return array('data'=>$result);
		}
		else{
			$total=$this->where($where)->count();
			if($total<1) return array();
			//默认取出数目
			if(!isset($options['counts'])){
				$options['counts']=DEFAULT_COUNTS;
			}
			//全部取出
			if(0==$options['counts']){
				$options['counts']=$total;
			}
			//默认排序
			if(!isset($options['order'])){
				$options['order']='dateline desc';
			}
			$this->where($where)->order($options['order']);
			$show=null;
			if(isset($options['pager'])){
				$page = new \Think\Page($total,$options['counts']);
				if(!empty($options['mid'])){
					//保证链接地址正确
					$page->setConfig('url', __APP__.'/mod/'.$options['mid']);
				}
				//修改配置信息
				default_pager($page);
				$show=$page->show();
				$result=$this->limit($page->firstRow.','.$page->listRows)->field($fields)->select();
			}else{
				$result=$this->limit($options['counts'])->field($fields)->select();
			}
			foreach ($result as $i=>$r){
				$result[$i]['date']=time2date($r['dateline']);
			}
			return array('page'=>$show,'data'=>$result);
		}
	}
	/**
	 * 关键字搜索
	 * @param string $keyword
	 * @return array
	 */
	function search($keyword){
		if(empty($keyword)) return array();
		$where['title']=array('LIKE',"%$keyword%");
		$where['mid']=array('NEQ',1);
		$total=$this->where($where)->count();
		if($total<1) return array();
		$this->where($where)->order('dateline desc');
		$page = new \Think\Page($total,DEFAULT_COUNTS);
		$page->setConfig('url', __APP__.'/search/keyword/'.$keyword);
		default_pager($page);
		$show=$page->show();
		$result=$this->limit($page->firstRow.','.$page->listRows)->field('id,mid,title,dateline,comments,views')->relation(true)->select();
		foreach ($result as $i=>$r){
			$r['date']=time2date($r['dateline']);
			$r['title']=str_replace($keyword,"<em>$keyword</em>", $r['title']);
			$result[$i]=$r;
		}
		return array('page'=>$show,'data'=>$result);
	}
}
?>
