<?php
namespace Home\Controller;

class ContentController extends IndexController {
	private $module=null;
	public function __construct(){
		parent::__construct();
	}
    
    /**
     * 输出当前位置导航
     * @param int $id
     */
    private function pos($id){
    	if(empty($id)) $this->error(L('NOT_EXISIT'));
//     	foreach($this->modules['array'] as $m){
//     		if($id==$m['id']){
//     			$this->module=$m;
//     			break;
//     		}
//     	}
		$this->module=M('Module')->getById($id);
    	if(is_null($this->module)){
    		$this->error(L('NOT_EXISIT'));
    	}
    	$pid=$this->module['module_pid'];
    	if(0!=$pid){
    		foreach($this->modules['array'] as $m){
    			if($m['id']==$pid){
    				$this->module['parent']=$m;
    				break;
    			}
    		}
    	}
    	$this->assign('module',$this->module);
    	
    	$this->left($id);
    }
    
    /**
     * 具体模块
     */
    public function module(){
    	$this->pos(I('get.mid'));
    	$result=D('Post')->get_posts(array('mid'=>I('get.mid'),'pager'=>true));
    	$this->assign('result',$result);
    	$this->display();
    }
    
    /**
     * 设置浏览量
     * @param int $tid
     */
    private function setViews($tid){
    	if(!cookie($tid)){
    		$return=M('Post')->where(array('id'=>$tid))->setInc('views',1);
    		if($return){
    			cookie($tid,1,60);
    		}
    	}
    }
    
    /**
     * 具体显示页面
     */
    public function showthread(){
    	$this->pos(I('get.mid'));
    	//是否显示文字标题
    	$this->assign('showtitle',1);
    	
    	$tid=I('get.tid',0,'int');
    	if(empty($tid)){
    		$this->error(L('NOT_EXISIT'));
    	}
    	$mid=I('get.mid');
    	//--默认版块不予显示--
    	if($mid==DEFAULT_MODULE){
    		$this->error(L('NOT_EXISIT'));
    	}
    	$result=D('Post')->get_posts(array('mid'=>$mid,'tid'=>I('get.tid'),'surround'=>true));
    	if(empty($result)||empty($result['data'])){
    		$this->error(L('NOT_EXISIT'));
    	}
    	$this->setViews($tid);
    	
    	$this->assign('result',$result);
    	$this->display();
    }
    /**
     * 静态页显示
     */
    public function showpost(){
    	$this->pos(I('get.mid'));
    	//是否显示文字标题
    	$this->assign('showtitle',0);
    	
    	//只允许设置链接的内容访问
    	if(empty($this->module['url'])){
    		$this->error(L('NOT_EXISIT'));
    	}
    	$tid=I('get.tid',0,'int');
    	if(empty($tid)){
    		$this->error(L('NOT_EXISIT'));
    	}
    	$result=D('Post')->get_posts(array('tid'=>I('get.tid')));
    	if(empty($result)||empty($result['data'])){
    		$this->error(L('NOT_EXISIT'));
    	}
    	$this->setViews($tid);
    	$this->assign('result',$result);
    	$this->display();
    }
    
    /**
     * 输出左侧导航
     * @param int $mid
     */
    private function left($mid){
    	if(empty($this->module)) return false;
    	$pid=$this->module['module_pid'];
    	$left=null;
    	if(0==$pid){
    		$pid=$mid;
    	}
    	foreach ($this->modules['hierarchy'] as $m) {
    		if(3==$this->module['module_level']){
    			foreach ($m['next'] as $n){
    				if($n['id']==$pid){
    					$left=$m;
    					//还会循环，但不出错
    					break;
    				}
    			}
    		}else{
    			if($m['id']==$pid){
    				$left=$m;
    				break;
    			}
    		}
    		
    	}
    	if(empty($left)){
    		$this->error(L('NOT_EXISIT'));
    	}
    	$this->assign('left',$left);
    }
}