<?php
namespace Home\Controller;
use Common\Controller\CommonController;

class IndexController extends CommonController {
	//顶部导航所有模块
	protected $modules;
	public function __construct(){
		parent::__construct();
		$this->modules=init_modules_cache();
		$this->assign('modules',$this->modules);
	}
	
	/*
	 * ------------------------------------------------------
	 * 定制内容
	 * ------------------------------------------------------
	 */
 	/**
	 * 右侧栏目
	 */
	private function top(){
		$top=M('Module')->where(array('push_position'=>TOP_PUSH_POSITION,'module_leaf'=>1))->order('position asc')->select();
		if(!empty($top)){
			foreach($top as $k=>$module){
				$top[$k]['data']=$this->getPush($module['id'],$module['push_count']);
			}
		}
		$this->assign('top',$top);
	}
	
	/**
	 * 左侧栏目
	 */
	private function news(){
		$news=array();
		foreach ($this->modules['array'] as $k=>$module){
			if(MAIN_PUSH_POSITION!=$module['push_position']){
				continue;
			}
			if(!empty($module['next'])){
				$mid=array($module['id']);
				foreach ($module['next'] as $n){
					if($n['module_leaf']==1){
						array_push($mid, $n['id']);
					}
				}
			}else{
				$mid=$module['id'];
			}
			$module['data']=$this->getPush($mid,$module['push_count']);
			unset($module['next']);
			array_push($news, $module);
		}
		$this->assign('news',$news);
	}
	/*
	 * ------------------------------------------------------
	* 定制内容结束
	* ------------------------------------------------------
	*/
	
	/**
	 * 输出幻灯
	 */
	private function banners(){
		//<include file="./Application/Common/View/banner.html" width="" height=""/>
		$banners=D('Admin/Banner')->get_banners();
		$this->assign('banners',$banners);
	}
	
	/**
	 * 获得推送的内容
	 * @param int|array $mid
	 * @param int $count
	 */
	private function getPush($mid,$count){
		if(empty($count)) return null;
		$result=D('Post')->get_posts(array('mid'=>$mid,'counts'=>$count));
		if(!empty($result))  return $result['data'];
		return null;
	}
	
	/**
	 * 网站首页
	 */
    public function index(){
    	//幻灯图片
    	$this->banners();
     	$this->top();
    	$this->news();
    	$this->friend_link();
    	$this->display();
    }
    
    /**
     * 搜索页面
     */
    public function search(){
    	$keyword=I('Get.keyword');
    	$ret=D('Post')->search($keyword);
    	$this->assign('post',$ret['data']);
    	$this->assign('pager',$ret['page']);
    	$this->display();
    }
}