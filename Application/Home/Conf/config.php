<?php
return array(
	// 加载扩展配置文件
	'LOAD_EXT_CONFIG' => 'home',
	
	// 路由的相关配置
	// 路由开启
	'URL_ROUTER_ON'	=>true,
	'URL_ROUTE_RULES'=>array(
		'mod/:mid\d'=>array('Home/Content/module'),
		't/:mid\d/:tid\d'=>array('Home/Content/showthread'),
		's/:mid\d/:tid\d'=>array('Home/Content/showpost'),
		'search'=>array('Home/Index/search'),
	),
	
	/* 默认主题 */
	'DEFAULT_THEME'    =>    'blue'
);
?>