<?php
namespace Common\Controller;

use Think\Controller;
use Think\Model;

/**
 * 网站公共抽象基类，包含公共模板和公共函数
 * 所有的控制器应继承此抽象类
 * 命名规则：
 * public和protected方法均采用下划线法则
 * 以_开始的protected方法一般可被多个子类继承使用
 * 私有方法均以驼峰法命名
 * 变量名称建议用驼峰法
 */
abstract class CommonController extends Controller {
	/**
	 * 构造函数：引入公共函数并修改语言包
	 */
	public function __construct(){
		parent::__construct();
		include_once COMMON_PATH.'Common/function_common.php';
		init_lang();
	}
	
	/**
	 * -----------------------------------------
	 * 公共函数部分
	 * -----------------------------------------
	 */
	/**
	 * 需要以post的ajax方式提交
	 * @param boolean $showErr 是否要返回提示错误
	 * @return boolean 以post的ajax方式提交返回true
	 */
	protected function _need_ajax_post($showErr=true){
		if(IS_AJAX&&IS_POST){
			if(!$showErr) return true;
		}else{
			if($showErr) $this->error(L('NOT_EXISIT'));
			else return false;
		}
	}
	/**
	 * 获得主键-元组对信息
	 * @param Model $dao
	 * @param string $key 主键名称，默认为id
	 * @return null|array
	 */
	protected function _get_key_tuple(&$dao,$key='id'){
		if($dao==null){
			return null;
		}
		$results=$dao->select();
		$ret=array();
		if(!empty($results)){
			foreach ($results as $result){
				$ret[$result[$key]]=$result;
			}
		}
		return $ret;
	}
	/**
	 * 获得主键-值对信息
	 * @param Model $dao
	 * @param string $key 主键名称，默认为id
	 * @param string $value 值的名称，默认为name
	 * @return null|array
	 */
	protected function _get_key_value(&$dao,$key='id',$value='name'){
		if($dao==null){
			return null;
		}
		$results=$dao->select();
		$ret=array();
		if(!empty($results)){
			foreach ($results as $result){
				$ret[$result[$key]]=$result[$value];
			}
		}
		return $ret;
	}
	
	/**
	 * -----------------------------------------
	 * 公共模块部分（子控制器可直接调用）
	 * -----------------------------------------
	 */
	/**
	 * -----------------------------------------
	 * 验证码模块
	 * -----------------------------------------
	 */
	/**
	 * 产生验证码
	 */
	public function verify(){
		$config = array(    
				'fontSize' => 14,    // 验证码字体大小    
				'length'   => 3,     // 验证码位数   
				'useNoise' => false, // 关闭验证码杂点
				'imageH'   => 30,
				'imageW'   => 85,
				'bg'	   => array(244,244,244)
		);
		$Verify = new \Think\Verify($config);
		$Verify->entry();
	}
	/**
	 * 检测验证码
	 */
	public function check_verify(){
		$this->_need_ajax_post();
		$verify = new \Think\Verify();
		$code = I('post.code');
		$id = I('post.id','');
		if(!$verify->check($code, $id)){
			$this->error(L('VERIFY_FAILURE'));
		}else{
			$this->success(L('VERIFY_SUCCESS'));
		}
	}
	
	public function friend_link(){
		$friendLink=get_cache('Link',false,'position asc');
		$this->assign('friendLink',$friendLink);
	}
}