<?php
return array(
		/*
		 应用一个新站点需要修改内容：
		 	语言包 Common/Lang/zh-cn.php 中的路径信息
			本配置文件中的数据库
		 添加后台管理内容：
		 	Admin/Controller/ 下对应的Controller
		 	Admin/View/ 下对应的模板文件
		 	Admin/Lang/zh-cn/ 下对应的语言包
		 	Public/StyleSheet/Admin/ 下对应的样式
		 	Public/JavaScript/Admin/ 下对应的JavaScript（可选）
		 	Public/Sql/app.sql 对应的数据库
		 	Public/Images/Admin/ 下对应的图片（如logo.jpg）
		 */
		
		
		/* 应用设定 */
    'MODULE_ALLOW_LIST' => array(
        'Common',
        'Home',
        'Admin'
    ),
		
		/* 类库扩展 */
		'AUTOLOAD_NAMESPACE' => array(
        'Util' => APP_PATH . 'Common/Common/Util'
    ),
		
		/* 数据库设置 */
		'DB_TYPE' => 'mysql', // 数据库类型
    'DB_HOST' => 'localhost', // 服务器地址
    'DB_NAME' => 'Basev1', // 数据库名
    'DB_USER' => 'root', // 用户名
    'DB_PWD' => '', // 密码
    'DB_PREFIX' => 'app_', // 前缀
    
    /* URL设置 */
    'URL_MODEL' => 1, // (REWRITE 模式)
                                   
    // 模板变量替换规则
    'TMPL_PARSE_STRING' => array(
        '__CSS__' => __ROOT__ . '/Public/StyleSheet',
        '__JS__' => __ROOT__ . '/Public/JavaScript',
        '__IMG__' => __ROOT__ . '/Public/Images',
        '__PLUGIN__' => __ROOT__ . '/Public/Plugins',
        '__XML__' => __ROOT__ . '/Public/Data/Xml'
    ),
    // /* 模板布局 */
    // 'LAYOUT_ON' => true,
    // 'LAYOUT_NAME' => 'layout',
    
    /* 语言包设置 */
    'LANG_SWITCH_ON' => true, // 开启语言包功能
    'LANG_AUTO_DETECT' => false, // 关闭语言的自动检测
    'LANG_LIST' => 'zh-cn', // 允许切换的语言列表 用逗号分隔
    
    /* 数据缓存设置 */
    'DATA_CACHE_TIME' => 6000, // 数据缓存有效期 0表示永久缓存
    'DATA_CACHE_COMPRESS' => false, // 数据缓存是否压缩缓存
    'DATA_CACHE_CHECK' => false, // 数据缓存是否校验缓存
    'DATA_CACHE_PREFIX' => '', // 缓存前缀
    'DATA_CACHE_TYPE' => 'File', // 数据缓存类型,支持:File|Db|Apc|Memcache|Shmop|Sqlite|Xcache|Apachenote|Eaccelerator
    'DATA_CACHE_PATH' => TEMP_PATH, // 缓存路径设置 (仅对File方式缓存有效)
    'DATA_CACHE_SUBDIR' => false, // 使用子目录缓存 (自动根据缓存标识的哈希创建子目录)
    'DATA_PATH_LEVEL' => 1, // 子目录缓存级别
    
    /* 调试配置 */
    // 显示页面的trace信息
    'SHOW_PAGE_TRACE' => false
);
?>