<?php
// /**
//  * 获得括号内外的内容
//  * @param string $str 括号必须为英文符号下的括号
//  * @param boolean $first 是否返回第一个括号内的内容，默认为true
//  * @return array
//  */
// function split_from_bracket($str,$first=true){
// 	$result=array();
// 	preg_match_all('/(?:\()(.*)(?:\))/i',$str,$result);
// 	if(!empty($result)){
// 		if($first){
// 			return $result[1][0];
// 		}
// 	}
// 	return $result;
// }
/**
 * 检查参数是否为空或者数组之中是否存在空值
 * @param mixed $var 参数
 * @return boolean 存在空值返回true，0返回false
 */
function hasnull($var){
	if(empty($var)&&$var!=0) return true;
	foreach ($var as $v){
		if(empty($v)&&$v!==0) return true;
	}	
	return false;
}
/**
 * 检查参数是否为空或者数组之中所有都是空
 * @param mixed $var 参数
 * @return boolean 所有都为空值返回true，0返回false
 */
function allnull($var){
	if(empty($var)&&$var!=0) return true;
	foreach ($var as $v){
		if(!empty($v)||$v===0) return false;
	}
	return true;
}
/**
 * 将时间戳转化为时间格式
 * @param int $time 时间戳
 * @param int $type 转化类型，默认为Y-m-d
 * @return string 
 */
function time2date($time,$type=0){
	switch($type){
		default:{
			return date('Y-m-d',$time);
		}
	}
}
/**
 * 公共页面分页配置
 * @param int $page 实例化的Page类
 * @param string $header 默认是 条记录
 */
function default_pager(&$page,$header=null){
	//显示的分页的数目
	$page->rollPage=5;
	$page->setConfig('first',L('FIRST_PAGE'));
	$page->setConfig('last',L('END_PAGE'));
	$page->setConfig('prev',L('UP_PAGE'));
	$page->setConfig('next',L('NEXT_PAGE'));
	$page->setConfig('theme', '<span class="pageRoll">%HEADER% </span> %FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END%');
	if($header){
		$page->setConfig('header',$header);
	}
}
/**
 * 初始化配置语言包
 * @param boolean $reset 是否强制重置
 */
function init_lang($reset=false){
	//使用缓存
	$lang=S('lang');
	if(empty($lang)||$reset){
		$lang=M('Word')->select();
		if(!empty($lang)){
			S('lang',$lang);
		}
	}
	if(!empty($lang)){
		//修改语言包设置
		foreach($lang as $l){
			if(!empty($l['value'])){
				L($l['lang'],$l['value']);
			}
		}
	}
}
/**
 * 初始化版块信息缓存
 * @return array
 */
function init_modules_cache($reset=false){
	$modules=S('modules');
	if(empty($modules['hierarchy'])||$reset){
		$modules=array();
		$dao=D('Admin/Module');
		$modules['hierarchy']=$dao->get_hierarchy_modules();
		$modules['array']=$dao->get_array_modules();
		if(!empty($modules)){
			S('modules',$modules);
		}
	}
	return $modules;
}
/**
 * 获得缓存信息
 * @return array
 */
function get_cache($table,$reset=false,$oder='id asc'){
	if(!isset($table)) return false;
	$data=S($table);
	if(empty($links)||$reset){
		$data=M($table)->order($oder)->select();
		if(!empty($data)){
			S($table,$data);
		}
	}
	return $data;
}
/**
 * 获得简介信息，抽取出其中的图片和视频信息
 * @param string $content 原内容，并未encode
 * @param int $counts 简介信息的字数，0则不取简介，直接获得图片信息
 * @param array $imgs （引用）图片信息array([0]=>array(图片信息),[1]=>array(图片链接地址))
 */
function get_introduction($content,$counts,&$imgs){
	//匹配出所有的图片信息
	//$preg="/<img.*src\s*=\s*[\"|\']?\s*([^>\"\'\s]*)/i";
	$preg='/\<img.*?src\=\"(.*?)\"[^>]*>/i';
	preg_match_all($preg,$content,$imgs);
	// 去除html的标签
	$content = strip_tags ( $content );
	//截取内容
	if (strlen ( $content ) > $counts && $counts>0) {
		//文字数目大于字数
		$content = mb_substr ( $content, 0, $counts, 'utf-8' );
	}
	if($content){
		return $content.'...';
	}else{
		return '';
	}
}
?>