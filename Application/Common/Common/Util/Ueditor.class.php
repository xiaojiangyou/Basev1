<?php
namespace Util;

use Util\Impl\EditorInterface;


/**
 * Ueditor编辑器实现类
 */
class Ueditor implements EditorInterface {
	/**
	 * 上传设置
	 */
	private $config=array(
			'type'=>'File',//I('get.type','File');
			'file'=>array(),//$_FILES['upfile']
	);
	
	public function __set($name,$value){
		if(isset($this->config[$name])){
			$this->config[$name]=$value;
		}
	}
	
	public function __get($name) {
		return $this->config[$name];
	}
	
	/**
	 * 获得最近上传的图片列表
	 */
	public function get_image($uid){
		$imgs=D('Img')->get_images($uid);
		$img='';
		foreach ($imgs as $i){
			$img.=UPLOAD_DIR.$i['path'].'b_'.$i['filename'].'ue_separate_ue';
		}
		return $img;
	}
	
	/**
	 * 解析视频
	 */
	public function parse_video(){
	}
	
	public function upload_file(){
		// 文件处理
		$upload = new \Think\Upload();// 实例化上传类
		$upload->rootPath=UPLOAD_DIR;
		switch ($this->type){
			case 'Image':{
				$upload->maxSize = 10*1024*1024; // 设置图片上传大小10M
				$upload->exts = array ('jpg','gif','png','jpeg'); // 设置上传类型
				$upload->autoSub = true;
				$upload->subName = array('date', 'Ym/d');
				$upload->savePath = 'Image/'; // 设置上传目录
				break;
			}
			default:{
				$upload->maxSize = 200*1024*1024;	// 文件上传大小200M
				if($this->type=='Video'){
					$upload->exts = array( 'swf', 'mkv', 'avi', 'rm', 'rmvb', 'mpeg', 'mpg', 'ogg', 'mov',
							'wmv', 'mp4', 'webm');
				}else{
					$upload->exts = array('rar' , 'doc' , 'docx' , 'zip' , 'pdf' , 'txt'); // 设置上传类型
				}
				$upload->autoSub = true;
				$upload->subName = array('date', 'Ym/d');
				$upload->savePath = $this->type.'/'; // 设置上传目录
			}
		}
		$info=$upload->uploadOne($this->file);
		if(!empty($info)){
			return $info;
		}else{
			return $upload->getError();
		}
	}
	/**
	 * 自动产生缩略图
	 * @param string $path 文件路径
	 * @param string $name 文件名
	 * @param int $count 缩略图的数量
	 * @param array $width 缩略图的宽度
	 * @param array $height 缩略图的高度
	 * @param array $prefix 缩略图前缀
	 * @param array $retType 返回类型的key
	 */
	public function auto_thumb($path,$name,$count=2,$width=array(600,200),$height=array(600,200),$prefix=array('b_','s_'),$retType=array('big','small')){
		$return=array();
		for($i=0;$i<$count;$i++){
			$image=new \Think\Image();
			$image->open(UPLOAD_DIR.$path.$name);
			$outpath=UPLOAD_DIR.$path.$prefix[$i].$name;
			$return[$retType[$i]]=$outpath;
			$image->thumb($width[$i], $height[$i])->save($outpath);
		}
		return $return;
	}
}

