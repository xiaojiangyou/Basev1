<?php
namespace Util\Impl;

/**
 * 文本编辑器接口
 */
interface EditorInterface{
	/**
	 * 上传文件
	 */
	public function upload_file();
}

