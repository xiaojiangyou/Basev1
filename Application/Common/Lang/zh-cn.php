<?php 
return array(
		'POST_SUCCESS'=>'内容提交成功！',
		'POST_FAILURE'=>'内容提交失败！',
		'UPDATE_SUCCESS'=>'内容修改成功！',
		'UPDATE_FAILURE'=>'内容修改失败！',
		'DELETE_SUCCESS'=>'内容删除成功！',
		'DELETE_FAILURE'=>'内容删除失败！',
		'NOT_EXISIT'=>'抱歉，您访问的内容不存在！',
		'NO_POWER'=>'无权进行此操作！',
		'UPLOAD_SUCCESS'=>'上传成功！',
		'UPLOAD_FAILURE'=>'上传失败！',
		
		/*登录注册相关*/
		'LOGIN'=>'登录',
		'USERNAME'=>'用户名：',
		'PASSWORD'=>'密&nbsp;&nbsp;&nbsp;码：',
		'VERIFY'=>'验证码：',
		'VERIFY_SUCCESS'=>'',
		'VERIFY_FAILURE'=>'请输入正确的验证码！',
		'CLICK_REFRESH'=>'点击刷新',
		
		/*常见语言设置*/
		'OK'=>'确&nbsp;&nbsp;定',
		'YES'=>'是',
		'NO'=>'否',
		'POST'=>'发布',
		'ADD'=>'新增',
		'INSERT'=>'插入',
		'RESET'=>'重置',
		'EDIT'=>'修改',
		'BACK'=>'还原',
		'LOGOUT'=>'退出',
		'NULL'=>'无',
		'CLOSE'=>'关闭',
		
		'INDEX'=>'首页',
		
		/*分页*/
		'FIRST_PAGE'=>'首页',
		'END_PAGE'=>'末页',
		'UP_PAGE'=>'上一页',
		'NEXT_PAGE'=>'下一页',
		
		/*公共提示*/
		'CANT_EMPTY'=>'提交内容不能为空！',
		
		/*网站配置*/
		'SITE_NAME'=>'[站点名称]',
		'SITE_KEYWORD'=>'',
		'SITE_DESCRIPTION'=>'',
		'COPYRIGHT'=>'版权所有 Copyright © 2014 All rights reserved',
		
		//修正图片根地址，放在语言包是为了可以输出到模版文件
		'WEB_ROOT'=>'http://localhost/basev1/',
		//后台部分内容管理
		'CONTENT_DEFAULT_URL'=>'http://localhost/basev1/index.php/t/',
		'CONTENT_REGEX'=>'/http:\/\/localhost\/basev1\/index.php\/t\/([0-9]*)\/([0-9]*)/',
);
?> 