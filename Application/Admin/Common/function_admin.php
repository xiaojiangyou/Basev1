<?php
/**
 * 初始化管理信息
 */
function init_admin(){
	$aid=session('aid');
	if(empty($aid)){
		session('aid',0);
		//当前登陆管理员信息
		session('admin',array());
		session('items',array());
		session('menu',array());
	}else{
		$admin=session('admin');
		if(empty($admin)){
			$admin=D('Admin')->relation(true)->getById($aid);
			session('admin',$admin);
		}
		init_items();
		init_menu();
	}
}
/**
 * 初始化所有权限项，及所有页面的权限（增删改）
 * @param boolean $reset 是否重置
 */
function init_items($reset=false){
	//所有的权限放入SESSION
	$items=session('items');
	if(empty($items)||$reset){
		$menu=new Admin\Controller\MenuController();
		//所有页面
		$items=$menu->items();
		$admin=session('admin');
		if(!empty($admin)){
			$items=D('Operation')->get_items_power($admin['group_id'],$items);
		}
		session('items',$items);
	}
}
/**
 * 初始化菜单编号对应项
 * @example array(Controller名称=>array(Action名称=>菜单id)
 * @param boolean $reset 是否重置
 * @return array
 */
function init_menu($reset=false){
	$menu=S('menu');
	if(empty($menu)||$reset){
		$menu=array();
		$items=session('items');
		foreach ($items as $id=>$item){
			if(!empty($item['tpl'])){
				$op=explode('/',$item['tpl']);
				if(!empty($op)&&count($op)==2){
					$menu[$op[0]][$op[1]]=$id;
				}
			}
		}
		S('menu',$menu);
	}
	return $menu;
}
// 迁移至公共函数
// /**
//  * 初始化版块信息缓存
//  * @return array
//  */
// function init_modules_cache($reset=false){
// 	$modules=S('modules');
// 	if(empty($modules)||$reset){
// 		$modules=array();
// 		$dao=D('Module');
// 		$modules['hierarchy']=$dao->get_hierarchy_modules();
// 		$modules['array']=$dao->get_array_modules();
// 		if(!empty($modules)){
// 			S('modules',$modules);
// 		}
// 	}
// 	return $modules;
// }
?>