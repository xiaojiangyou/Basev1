<?php
namespace Admin\Model;
use Think\Model;

//模块默认排序方式 ModuleModel
define('DEFAULT_MODULE_ORDER','position asc');
define('MODULE_SPECIAL_CONTENT_URL',__APP__.'/s/');

class ModuleModel extends Model {
	/**
	 * 自动验证
	 */
	protected $_validate = array(
		array('name','require','{%MODULE_NAME_UNIQUE}',Model::EXISTS_VALIDATE,'',Model::MODEL_INSERT),
	);
	
	/**
	 * 获得版块数组结构信息
	 */
	public function get_array_modules(){
		$hierarchyModules=$this->get_hierarchy_modules();
		return $this->arrayModules($hierarchyModules);
	}
	/**
	 * 获得子节点键值对
	 * @param boolean $isLeaf 获得最终叶子节点
	 */
	public function get_key_tuple($isLeaf=false,$order=DEFAULT_MODULE_ORDER){
		if(isset($isLeaf)&&false!==$isLeaf) $this->where(array('module_leaf'=>$isLeaf));
		$modules=$this->order($order)->select();
		$ret=array();
		foreach ($modules as $m) {
			$ret[$m['id']]=$m;
		}
		return $ret;
	}
	/**
	 * 获得版块层次结构信息
	 * @param int $level 版块层次
	 */
	public function get_hierarchy_modules($order=DEFAULT_MODULE_ORDER){
		$modules=$this->order($order)->select();
		$retModules=array();
		return $this->hierarchyModules($modules);
	}
	/**
	 * 版块层次排序为数组（递归）
	 * @param array $hierarchyModules 该层次的版块数组
	 */
	private function arrayModules($hierarchyModules){
		$ret=array();
		if(empty($hierarchyModules)){
			return $ret;
		}
		foreach ($hierarchyModules as $n){
			$next=$n['next'];
			unset($n['next']);
			array_push($ret,$n);
			if(!empty($next)){
				$ret=array_merge($ret,$this->arrayModules($next));
			}
		}
		return $ret;
	}
	
	/**
	 * 对版块层次排序（递归）
	 * @param array $modules 所有层次
	 * @param int $pid 上层节点
	 * @param int $level 版块层次
	 */
	private function hierarchyModules(&$modules,$pid=0,$level=1){
		$ret=array();
		$current=0;
		if(empty($modules)) return $ret;
		foreach($modules as $k=>$m){
			$this->parseUrl($m);
			if($level==$m['module_level']&&$pid==$m['module_pid']){
				$ret[$current++]=$m;
				unset($modules[$k]);
			}
		}
		if(!empty($ret)){
			foreach ($ret as $i=>$r){
				$ret[$i]['next']=$this->hierarchyModules($modules,$r['id'],$level+1);
			}
		}
		return $ret;
	}
	/**
	 * 解析URL
	 */
	private function parseUrl(&$module){
		if(!empty($module['url'])&&is_numeric($module['url'])){
			$module['url']=MODULE_SPECIAL_CONTENT_URL.$module['id'].'/'.$module['url'];
		}
	}
}
?>
