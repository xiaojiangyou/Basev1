<?php
namespace Admin\Model;

use Think\Model;
use Think\Model\RelationModel;

class AdminModel extends RelationModel {
	/**
	 * 自动验证条件
	 * @var array()
	 */
	protected $_validate = array(
		/**
		 * -----------------------------------
		 * 修改数据时unique方法存在缺陷
		 * -----------------------------------
		 */
		array('username','require','{%USER_EXISIT}',Model::EXISTS_VALIDATE,'unique',Model::MODEL_INSERT),
		/**
		 * 验证密码报错
		 */
		array('password','check_pwd','{%PASS_EMPTY}',Model::MUST_VALIDATE,'callback',Model::MODEL_INSERT),
	);
	/**
	 * 验证密码
	 * @param string $password
	 * @return boolean
	 */
	protected function check_pwd($password){
		if(isset($password)&&empty($password)){
			return false;
		}else{
			return true;
		}
	}
	/**
	 * 关联条件
	 * @var array()
	 */
	protected $_link = array(
			'Group'=> self::BELONGS_TO
	);
	/**
	 * 检查登陆
	 * @param string $username
	 * @param string $password
	 */
	public function check($username,$password){
		if(!$username||!$password){
			$this->error=L('USER_OR_PASS_EMPTY');
			return false;
		}
		$where['username']=$username;
		$admin=$this->where($where)->find();
		if(empty($admin)){
			$this->error=L('USER_EMPTY');
			return false;
		}else{
			if(md5($password)!=$admin['password']){
				$this->error=L('PASS_ERROR');
				return false;
			}else{
				session('aid',$admin['id']);
				return true;
			}
		}
	}
}
?>
