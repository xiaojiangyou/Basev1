<?php
namespace Admin\Model;
use Think\Model;

class FileModel extends Model {
	/**
	 * 更新上传的文件
	 * @param array $info 文件上传信息
	 */
	public function add_upload($info){
		if(empty($info)){
			return false;
		}
		$data=array();
		$data['uid']=$info['uid'];
		$data['name']=$info['name'];
		$data['path']=$info['savepath'];
		$data['filename']=$info['savename'];
		$data['size']=$info['size'];
		$data['ext']=$info['ext'];
		$data['description']='';
		$data['dateline']=NOW_TIME;
		return $this->add($data);
	}
}
?>
