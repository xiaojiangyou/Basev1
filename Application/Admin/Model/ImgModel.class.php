<?php
namespace Admin\Model;
use Think\Model;

class ImgModel extends Model {
	/**
	 * 更新上传的文件
	 * @param array $info 图片上传信息
	 * @param int $aid 相册编号
	 */
	public function add_upload($info,$aid=0){
		if(empty($info)){
			return false;
		}
		$data=array();
		$data['aid']=$aid;
		$data['uid']=$info['uid'];
		$data['name']=$info['name'];
		$data['path']=$info['savepath'];
		$data['filename']=$info['savename'];
		$data['size']=$info['size'];
		$data['ext']=$info['ext'];
		$data['description']=$info['description'];
		$data['dateline']=NOW_TIME;
		return $this->add($data);
	}
	/**
	 * 获得图片信息
	 * @param int $uid
	 * @param int $aid
	 * @param string $order
	 * @param string|int $limit
	 */
	public function get_images($uid,$aid=0,$order='dateline desc',$limit=30){
		if(!isset($uid))
			return null;
		else
			return $this->where(array('uid'=>$uid,'aid'=>$aid))->order($order)->limit($limit)->select();
	}
}
?>
