<?php
namespace Admin\Model;
use Think\Model;

class OperationModel extends Model {
	/**
	 * 获得操作权限
	 * @param int $group_id
	 * @param array $items
	 * @return 
	 */
	public function get_items_power($group_id,$items){
		if(!isset($items)){
			$items=session('items');
		}
		if(empty($items)) return false;
		$operation=$this->where(array('group_id'=>$group_id))->find();
		if(empty($operation)){
			foreach ($items as $k=>$item){
				$item['edit']=1;
				$item['add']=1;
				$item['del']=1;
				$items[$k]=$item;
			}
		}else{
			$index=0;
			foreach ($items as $k=>$item){
				$item['edit']=$operation['edit'][$index];
				$item['add']=$operation['add'][$index];
				$item['del']=$operation['del'][$index];
				$items[$k]=$item;
				$index++;
			}
		}
		return $items;
	}
	/**
	 * 获得指定组的操作权限
	 * @param int $group_id
	 */
	public function get_operation($group_id){
		$items=$this->get_items_power($group_id);
		if(empty($items)) return null;
		foreach ($items as $k=>$v){
			unset($items[$k]['name']);
			unset($items[$k]['tpl']);
		}
		return $items;
	}
}
?>
