<?php
namespace Admin\Model;

use Think\Model;
use Think\Model\RelationModel;

class BannerModel extends RelationModel {
	/**
	 * 获得指定的版块的幻灯信息
	 * @param string $belong
	 */
	public function get_banners($belong='index'){
		return $this->where(array('belong'=>$belong))->order('position asc')->select();
	}
}
?>
