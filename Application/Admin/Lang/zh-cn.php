<?php 
return array(
		'HTML_TITLE_ADMIN'=>'后台管理中心',
		
		/* 页面相关 */
		'HEADER_LOGO'=>"",
		'HEADER_TITLE'=>'后台管理中心',
		'HEADER_WELCOME'=>'欢迎您，',
		'CLOSE_ALL'=>'关闭所有',
		
		//公共提示
		'ID_EXISIT'=>'该编号已存在！',
		// 技巧提示标题
		'TIP_H3'=>'<h3>技巧提示</h3>',
		'NO_POWER_TIP'=>'<b class="tip">您无权进行此操作！</b>',
		'NEED_LOGIN'=>'请登录后再进行此操作！',
		
		/* Index */
		'WELCOME'=>'欢迎',
		// 登录页面
		'LOGIN_HEADER'=>'后台管理中心',
		'USER_OR_PASS_EMPTY'=>'用户名或密码为空！',
		'USER_EMPTY'=>'管理员不存在！',
		'PASS_ERROR'=>'密码不正确！',
		'LOGIN_SUCCESS'=>'登录成功，正在跳转...',
		//'LOGIN_FAIL'=>'验证失败，请重试！',
		
);
?> 