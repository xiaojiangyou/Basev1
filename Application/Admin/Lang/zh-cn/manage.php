<?php 
return array(
		/* 管理组管理 group */
		'GROUP_CAPTION'=>'管理组列表',
		'GROUP_COL_NAMES'=>'"编号","管理组","管理级别","操作"',
		'GROUP_TIP'=>'
			<p>&bull;&nbsp;本页可对管理组和管理员进行设置和修改；</p>
			<p>&bull;&nbsp;出于安全考虑，管理员的<b>登录密码</b>进行了加密处理。<em>编辑</em>管理员时密码留空则表示保持原密码不变；</p>
			<p>&bull;&nbsp;如需修改管理员分组或批量删除管理员，请至<b>管理团队&rarr;管理员</b>；</p>
			<p>&bull;&nbsp;权限越高的管理员的<b>管理级别</b>（数字）应越小，低权限管理员无法操作高权限管理员的内容；</p>
			<p>&bull;&nbsp;删除管理组之前请先删除相应管理组的所有管理员；
			<p>&bull;&nbsp;一旦删除管理组或管理员将<em>无法恢复</em>，请谨慎操作！</p>'
		,
		'MUST_DEL_ADMIN'=>'请先删除该管理组的所有管理员！',
		
		/* 管理员管理 admin */
		'ADMIN_CAPTION'=>'管理员列表',
		'ADMIN_COL_NAMES'=>'"编号","用户名","真实姓名","登录密码","管理组"',
		'ADMIN_TIP'=>'
			<p>&bull;&nbsp;本页可设置和修改管理员，修改管理员分组只可在本页完成；</p>
			<p>&bull;&nbsp;出于安全考虑，<b>登录密码</b>进行了加密处理。<em>编辑</em>管理员时密码留空则表示保持原密码不变；</p>
			<p>&bull;&nbsp;本页可批量删除管理员，一旦删除将<em>无法恢复</em>，请谨慎操作！</p>'
		,
		'USER_EXISIT'=>'该用户名已存在，无法添加！',
		'PASS_EMPTY'=>'密码不能为空！',
		
		/* 菜单权限 power */
		'SET_GROUP'=>'<em class="tip">注意：请先设置管理组才能设置相应权限</em>',
		'SELECT_GROUP'=>'请选择管理组：',
		'POWER_TIP'=>
			'<p>&bull;&nbsp;本页可对不同管理组的管理菜单权限进行编辑，该权限只影响页面是否可见；</p>
			<p>&bull;&nbsp;如果子节点设置有权限，其父节点必须也设置有权限；</p>
			<p>&bull;&nbsp;默认情况下，除特别指定需要配置权限的菜单外，所有下层子菜单的权限将继承上层菜单的权限。</p>'
		,
		
		/* 操作权限 operation */
		'MENU_OPERATION'=>'可用菜单项',
		'ADD_OPERATION'=>'新增权限',
		'DEL_OPERATION'=>'删除权限',
		'EDIT_OPERATION'=>'修改权限',
		'SELECT_ALL'=>'全选：',
		'SET_ALL_OPERATION'=>'设置全部',
		'SET_ALL_ERROR'=>'请为所有的菜单项设置操作权限！',
		'OPERATION_TIP'=>'
			<p>&bull;&nbsp;本页可对不同管理组的操作权限进行编辑，该权限影响对应页面的相关操作是否可用；</p>
			<p>&bull;&nbsp;默认状态下所有管理组拥有所有的操作权限；</p>
			<p class="tip">&bull;&nbsp;注意：一旦菜单发生变动，相应的操作权限也必须重新设置；</p>
			<p>&bull;&nbsp;某些菜单项由于安全设置默认关闭了部分权限，此时再设置权限不会产生作用。</p>
		',
		
);
?> 