<?php 
return array(
		'VIEW_MAP'=>'查看地图',
		
		/* 企业信息 enterprise */
		'ENTERPRISE_CAPTION'=>'企业列表',
		'ENTERPRISE_COL_NAMES'=>'"企业编号","企业名称","企业地址","纬度","经度","企业描述","企业图片"',
		'ENTERPRISE_TIP'=>'
			<p>&bull;&nbsp;本页可管理所有企业信息；</p>
			<p>&bull;&nbsp;点击选择地址可在地图上搜索和选择企业地址；</p>
		',
		
		'POLLUTION_CAPTION'=>'排污口列表',
		'POLLUTION_COL_NAMES'=>'"排污口编号","所属企业","排污口地址","纬度","经度","排污口描述","收录时间"',
		'POLLUTION_TIP'=>'
			<p>&bull;&nbsp;本页可管理所有排污口信息；</p>
			<p>&bull;&nbsp;点击选择地址可在地图上搜索和选择排污口地址；</p>
		',
);
?> 