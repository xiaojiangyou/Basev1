<?php 
return array(
		/* 版块管理 module */
		'MODULE_CAPTION'=>'版块列表',
		'MODULE_COL_NAMES'=>'"版块名称","版块编号","版块介绍","版块层次","父版块编号","最终版块","操作","导航","版块链接","版块图片","发布内容","导航/推送位置","显示顺序","推送数目","是否允许评论",',
		'MODULE_TIP'=>'
			<p>&bull;&nbsp;本页可设置和修改网站子版块；</p>
			<p>&bull;&nbsp;<b>版块编号</b>自动生成，不可修改；</p>
			<p>&bull;&nbsp;<b>版块层次</b>最小设置为 1，表示主版块，其他子版块<b>版块层次</b>比上层版块层次 +1（原则上版块层次不要超过 2）；</p>
			<p>&bull;&nbsp;<b>父版块编号</b>使用上层版块的版块编号，请确保其存在；</p>
			<p>&bull;&nbsp;<b>最终版块</b>为“是”的版块无法进行<em>添加子版块</em><span class="ui-icon ui-icon-carat-1-s"></span>操作；</p>
			<p>&bull;&nbsp;设置“可见”的<b>导航</b>显示于首页等导航位置；</p>
			<p>&bull;&nbsp;设置<b>版块链接</b>的版块将使用该链接作为其地址；</p>
			<p>&bull;&nbsp;设置<b>版块图片</b>的版块可使用该图片作为其版块描述图片；</p>
			<p>&bull;&nbsp;<b>发布内容</b>设置“是”的版块（或子版块）可以发布新文章；</p>
			<p>&bull;&nbsp;<b>导航/推送位置</b>使用数字表示，留空表示默认为 0；</p>
			<p>&bull;&nbsp;<b>显示顺序</b>使用数字表示，数字越小越靠前显示，留空表示默认为 0；</p>
			<p>&bull;&nbsp;<b>推送数目</b>表示每个版块推送的动态条数，留空表示默认为 0；</p>
			<p>&bull;&nbsp;删除版块之后，其对应版块的内容将<em>无法显示</em>，请谨慎操作！</p>
		',
		'MODULE_NAME_UNIQUE'=>'版块名已存在，无法添加！',
		
		/* 语言设置 word */
		'WORD_CAPTION'=>'语言包列表',
		'WORD_COL_NAMES'=>'"语言编号","说明","语言代码","语言内容"',
		'WORD_TIP'=>'
			<p>&bull;&nbsp;本页可设置和修改网站语言包；</p>
			<p>&bull;&nbsp;如不设置，网站将采用其默认语言包设置；</p>
			<p>&bull;&nbsp;当前常见已有语言设置如下：</p>
			<p>&nbsp;- 站点名称：[站点名称]</p>
			<p>&bull;&nbsp;如需获得语言包代码，请联系系统管理员。</p>
		',
		
		/* 内容发布 post_new */
		'SET_MODULE'=>'<p class="tip">注意：请先到<b>页面设置>版块管理</b>中设置版块信息！</p>',
		'CONTENT_TITLE'=>'内容标题：',
		'BELONG_MODULE'=>'所属版块：',
		'RESELECT'=>'重选版块：',
		'POST_NEW_TIP'=>'
			<p>&bull;&nbsp;本页可发布指定版块的新内容；</p>
			<p>&bull;&nbsp;本页没有自动保存功能，建议您在Word中将文本排好版之后粘贴入编辑框发布；</p>
			<p>&bull;&nbsp;为保证页面结构，标题最多可输入50个字符；</p>
			<p>&bull;&nbsp;注意：所有只有一个页面的版块（例如联系我们）的所属<b>板块名称</b>请选择“默认”</p>
		',
		
		/* 内容列表 post_list */
		'POST_LIST_CAPTION'=>'文章列表',
		'POST_LIST_COL_NAMES'=>'"帖子编号","版块名称","发布人","标题","内容","发布时间","标红","评论数","点击数","更多操作"',
		'POST_LIST_TIP'=>'
			<p>&bull;&nbsp;本页可以查看和简单编辑各个版块发布的文章内容；</p>
			<p>&bull;&nbsp;您可以修改文章内容的标题，也可以指定文章内容的版块归属；</p>
			<p>&bull;&nbsp;<b>更多操作</b>中，点击<span class="ui-icon ui-icon-document"></span>可查看文章内容；
			点击<span class="ui-icon ui-icon-pencil"></span>可详细编辑发布的文章内容，等同于<b>内容管理&rarr;文章编辑</b>；</p>
			<p>&bull;&nbsp;删除文章内容操作<em>无法恢复</em>，请谨慎操作！</p>
			<p>&bull;&nbsp;注意：所有只有一个页面的版块（例如联系我们）的所属<b>板块名称</b>请选择“默认”</p>
		',
		
		/* 内容编辑 post_edit */
		'CONTENT_URL'=>'文章网址：',
		'CONTENT_IMPORT'=>'导入内容',
// 		'CONTENT_DEFAULT_URL'=>'http://localhost/base/index.php/content/',
// 		'CONTENT_REGEX'=>'/http:\/\/localhost\/base\/index.php\/content\/([0-9]*)\/([0-9]*)/',
		'POST_EDIT_TIP'=>'
			<p>&bull;&nbsp;本页可编辑各个版块的文章；</p>
			<p>&bull;&nbsp;在文章列表页中点击编辑，即可对相应的内容进行修改；</p>
			<p>&bull;&nbsp;可以将内容帖子对应的地址复制贴入内容网址中，点击<b>导入内容</b>即可导入相应的内容；</p>
			<p>&bull;&nbsp;为保证界面结构，标题最多可输入50个字符；</p>
			<p>&bull;&nbsp;一旦修改，将<em>不可恢复</em>，请谨慎操作！</p>
			<p>&bull;&nbsp;注意：所有只有一个页面的版块（例如联系我们）的所属<b>板块名称</b>请选择“默认”</p>
		',
		
		/* 幻灯列表 banner_list */
		'BANNER_LIST_CAPTION'=>'幻灯列表',
		'BANNER_LIST_COL_NAMES'=>'"幻灯编号","图片地址","链接地址","说明文字","是否显示","显示顺序","归属页面","查看图片"',
		'BANNER_LIST_TIP'=>'
			<p>&bull;&nbsp;本页可对网站各处幻灯进行设置和修改；</p>
			<p>&bull;&nbsp;<b>显示顺序</b>以数字表示，数字越小将越靠前显示；</p>
			<p>&bull;&nbsp;<b>图片地址</b>可从现有发布的文章中获取，也可以至上传幻灯管理项上传自定义图片；</p>
			<p>&bull;&nbsp;<b>链接地址</b>表示点击图片查看的具体内容（可留空）；</p>
			<p>&bull;&nbsp;<b>说明文字</b>表示幻灯下方显示的文字说明（可留空）；</p>
			<p>&bull;&nbsp;允许显示的内容将显示在对应页面幻灯区域；</p>
			<p>&bull;&nbsp;<b>归属页面</b>默认为首页幻灯；</p>
			<p>&bull;&nbsp;<b>查看图片</b>中，点击<span class="ui-icon ui-icon-image"></span>可查看幻灯图片。</p>
		',
		
		/* 上传幻灯 banner_new */
		'UPLOAD_IMAGE'=>'上传图片：',
		'CLICK_UPLOAD'=>'点击上传',
		'IMAGE_PREVIEW'=>'图片预览：',
		'WAIT_UPLOAD'=>'等待上传...',
		'LINK'=>'链接地址：',
		'IMAGE_TITLE'=>'说明文字：',
		'IMAGE_BELONG'=>'归属页面：',
		'DEFAULT_BELONG'=>'&nbsp;（默认归属为首页index）',
		'IMAGE_POSITION'=>'显示顺序：',
		'ADD_BANNER'=>'添加幻灯',
		'BANNER_NEW_TIP'=>'
			<p>&bull;&nbsp;本页可手动上传幻灯图片，并编辑相关信息；</p>
			<p>&bull;&nbsp;除图片外的所有选项均可留空；</p>
			<p>&bull;&nbsp;如需修改幻灯信息请到幻灯列表管理项。</p>
		',
		
		/* 相册列表 album */
		'ALBUM_CAPTION'=>'相册列表',
		'ALBUM_COL_NAMES'=>'"相册编号","相册名称","上传者编号","相册描述","相册位置","是否官方","是否公共","是否可见","图片数目","创建时间","封面图片"',
		'ALBUM_TIP'=>'
			<p>&bull;&nbsp;本页可管理网站所有相册信息；</p>
			<p>&bull;&nbsp;<b>官方相册</b>可用于显示在网站公共页面；</p>
			<p>&bull;&nbsp;<b>公共相册</b>为所有用户均可上传使用的相册。</p>
		',
		
		/* 图片列表 img_list */
		'IMG_LIST_CAPTION'=>'图片列表',
		'IMG_LIST_COL_NAMES'=>'"图片编号","所属相册","上传者编号","图片名称","图片大小 (KB)","图片描述","上传时间","缩略图","预览"',
		'IMG_LIST_TIP'=>'
			<p>&bull;&nbsp;本页可管理网站所有图片信息；</p>
			<p>&bull;&nbsp;<b>图片名称</b>默认为上传时图片的文件名；</p>
			<p>&bull;&nbsp;<b>预览</b>中，点击<span class="ui-icon ui-icon-image"></span>可预览图片。</p>
		',
		
		'LINK_CAPTION'=>'友情链接',
		'LINK_COL_NAMES'=>'"链接编号","链接地址","链接说明","链接网站图标","是否显示","显示顺序"',
		'LINK_TIP'=>'
			<p>&bull;&nbsp;本页可管理网站友情链接；</p>
			<p>&bull;&nbsp;<b>链接网站图标</b>为友情链接网站的图标地址，可留空。</p>
		',
);
?> 