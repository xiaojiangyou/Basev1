<?php
namespace Admin\Controller;

/**
 * 管理设置相关表格实现子类
 */
class ManageController extends JqGridController{
	/**
	 * 当前登陆的管理员的管理组
	 * @var array()
	 */
	private $group;
	/**
	 * 构造函数
	 */
	public function __construct(){
		parent::__construct();
		$admin=session('admin');
		$this->group=$admin['Group'];
	}
	/**
	 * ------------------------------------------
	 * 管理员管理
	 * ------------------------------------------
	 */
	/**
	 * 管理员列表
	 */
	public function admin(){
		if($this->_need_ajax_post(false)){
			$this->dao=M('Admin');
			$groups=M('Group')->where('level >= '.$this->group['level'])->select();
			$groupIds=array();
			foreach($groups as $k=>$group){
				$groupIds[$k]=$group['id'];
			}
			$group_id=I('get.group_id',null,'int');
			if($group_id) $condition['group_id']=$group_id;
			else $condition['group_id']=array('in',$groupIds);
			echo $this->select(array('condition'=>$condition));
		}else{
			$this->_grid_display();
		}
	}
	/**
	 * 管理员操作
	 */
	public function admin_oper(){
		$this->_need_ajax_post();
		$this->dao=D('Admin');
		$this->data['username']=I('post.username');
		$password=I('post.password',null);
		if($password) $this->data['password']=md5($password);
		$this->data['name']=I('post.name');
		$group_id=I('get.group_id',null,'int');
		if($group_id) $this->data['group_id']=$group_id;
		else $this->data['group_id']=I('post.group_id',null,'int');
		$this->oper();
	}
	/**
	 * ------------------------------------------
	 * 管理组管理
	 * ------------------------------------------
	 */
	/**
	 * 管理组列表
	 */
	public function group(){
		if($this->_need_ajax_post(false)){
			$this->dao=M('Group');
			//--只可查询比当前管理组权限低的管理组--
			echo $this->select(array('fields'=>'id as group_id,name,level',
					'condition'=>array('level'=>array('EGT',$this->group['level']))));
		}else{
			$this->_grid_display();
		}
	}
	/**
	 * 管理组操作
	 */
	public function group_oper(){
		$this->_need_ajax_post();
		$this->dao=M('Group');
		//系统管理员无法修改
		if($this->id==SYSTEM_GROUP){
			$this->error(L('NO_POWER'));
		}else{
			if($this->oper=='del'){
				$admins=M('Admin')->where('group_id='.$this->id)->select();
				if(!empty($admins)){
					$this->error(L('MUST_DEL_ADMIN'));
					return;
				}
			}
			$this->data['name']=I('post.name');
			$this->data['level']=I('post.level');
			$this->oper();
		}
	}
	/**
	 * ------------------------------------------
	 * 权限管理
	 * ------------------------------------------
	 */
	/**
	 * 页面权限
	 */
	public function power(){
		if($this->_need_ajax_post(false)){
			if($this->oper=='get_power'){
				$this->ajaxReturn($this->getPower(I('post.group_id')));
			}else{
				//当前管理组
				$group_id=I('post.group_id');
				if(!$group_id||$group_id==SYSTEM_GROUP){
					$this->error(L('NO_POWER'));
					return;
				}
				//前台是序列化提交，需要获得键名
				$keys=array_keys($_POST);
				$dao=M('Power');
				foreach ($keys as $k){
					$data['group_id']=$group_id;
					$pk=explode('_',$k);
					//是权限的参数
					if($pk[0]=='power'){
						$data['power_id']=$pk[1];
						$data['has_power']=I("post.$k",0);
						$where['group_id']=$group_id;
						$where['power_id']=$data['power_id'];
						if($dao->add($data)||$dao->where($where)->save($data)!==false){
							continue;
						}
						else{
							$this->error(L('UPDATE_FAILURE'));
							return;
						}
					}
				}
				//修改权限之后重置XML
				$menu=new MenuController();
				$menu->reset_xml($group_id);
				$this->success(L('UPDATE_SUCCESS'));
			}
		}else{
			$this->_need_edit();
			//获得管理组
			//--只能管理比当前管理组权限低的管理组--
			$dao=M('Group')->where('level >= '.$this->group['level']);
			$group=$this->_get_key_tuple($dao);
			unset($group[SYSTEM_GROUP]);
			//获得所有需要管理的权限列表
			$menu=new MenuController();
			$powers=$menu->powers();
			//获得第一个管理组
			$firstGroup=array_slice($group, 0,1);
			$hasPower=$this->getPower($firstGroup[0]['id']);
			$this->assign('group',$group);
			$this->assign('powers',$powers);
			$this->assign('hasPower',$hasPower);
			
			layout(false);
			$this->display();
		}
	}
	/**
	 * 操作权限
	 */
	public function operation(){
		if($this->_need_ajax_post(false)){
			if($this->oper=='get_operation'){
				$this->ajaxReturn(D('Operation')->get_operation(I('post.group_id')));
			}else{
				//当前管理组
				$group_id=I('post.group_id');
				if(!$group_id||$group_id==SYSTEM_GROUP){
					$this->error(L('NO_POWER'));
					return;
				}
				$dao=M('Operation');
				$items=session('items');
				$len=count($items);
				$data['add']=I('post.add');
				$data['del']=I('post.del');
				$data['edit']=I('post.edit');
				foreach ($data as $d){
					if($len!=strlen($d)){
						$this->error(L('SET_ALL_ERROR'));
						return;
					}
				}
				$where=array('group_id'=>$group_id);
				$dao->where($where)->delete();
				$data['group_id']=$group_id;
				$ret=$dao->add($data);
				if($ret){
					$this->success(L('UPDATE_SUCCESS'));
				}else{
					$this->error(L('UPDATE_FAILURE'));
				}
			}
		}
		else{
			$this->_need_edit();
			//获得管理组
			//--只能管理比当前管理组权限低的管理组--
			$dao=M('Group')->where('level >= '.$this->group['level']);
			$group=$this->_get_key_tuple($dao);
			unset($group[SYSTEM_GROUP]);
			$this->assign('group',$group);
			$this->assign('items',session('items'));
			
			layout(false);
			$this->display();
		}
	}
	/**
	 * 根据管理组获得权限信息
	 * @param int $group_id
	 * @return array()
	 */
	private function getPower($group_id){
		$dao=M('Power');
		if (empty($group_id)){
			return array();
		}
		else{
			$dao->where(array('group_id'=>$group_id));
			return $this->_get_key_value($dao,'power_id','has_power');
		}
	}
}