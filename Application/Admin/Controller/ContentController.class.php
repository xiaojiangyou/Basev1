<?php
namespace Admin\Controller;

define('DEFAULT_PUSH_COUNT',10);
define('DEFAULT_BANNER_BELONG','index');
define('DEFAULT_INTRODUCTION_COUNT',200);
/**
 * 内容管理
 */
class ContentController extends JqGridController{
	/**
	 * ------------------------------------------
	 * 版块管理
	 * ------------------------------------------
	 */
	/**
	 * 版块列表
	 */
	public function module(){
		if($this->_need_ajax_post(false)){
			$this->dao=D('Module');
			$rows=$this->dao->get_array_modules();
			$ret=array('total'=>1,'records'=>count($rows),'page'=>1);
			foreach($rows as $k=>$row){
				//显示为树形结构
				$row['isLeaf']=$row['module_leaf']==1?true:false;
				$row['parent']=$row['module_pid']==0?"null":$row['module_pid'];
				$row['level']=$row['module_level'];
				$row['expanded']=true;
				$row['loaded']=true;
				$row['more']=$row['module_leaf'];
				$ret['rows'][$k]=$row;
			}
			echo $this->ajaxReturn($ret);
		}else{
			$this->_grid_display('full');
		}
	}
	
	/**
	 * 版块操作
	 */
	public function module_oper(){
		$this->_need_ajax_post();
		$this->dao=D('Admin/Module');
		$this->data['name']=I('post.name');//必须
		//$this->data['description']=I('post.description');
		$this->data['description']=$_POST['description'];
		$this->data['position']=I('post.position',0,'int');
		$this->data['seen']=I('post.seen',0,'int');
		$this->data['allow_comment']=I('post.allow_comment',0,'int');
		$this->data['url']=I('post.url');
		$this->data['img']=I('post.img');
		$this->data['push_position']=I('post.push_position',0,'int');
		$this->data['contents']=I('post.contents',1,'int');
		$this->data['push_count']=I('post.push_count',DEFAULT_PUSH_COUNT,'int');
		
		$this->data['module_level']=I('post.module_level',1,'int');//必须
		$this->data['module_pid']=I('post.module_pid',0,'int');//必须
		$this->data['module_leaf']=I('post.module_leaf',0,'int');//必须
		$callback=array('function'=>'init_modules_cache','params'=>true);
		$this->oper(array('success'=>$callback));
	}
	/**
	 * ---------------------------------------------
	 * 内容相关
	 * ---------------------------------------------
	 */
	
// 	/**
// 	 * 获得指定版块的层次
// 	 * @param array $modules
// 	 * @param int $id
// 	 * @param int $level
// 	 */
// 	private function getHierarchyModule($modules,$id){
// 		$ret=array();
// 		if(0==$id){
// 			return;
// 		}else{
// 			foreach($modules as $m){
// 				if($id==$m['id']){
// 					$ret=$m;
// 					$ret['parent']=$this->getHierarchyModule($modules,$m['module_pid']);
// 					break;
// 				}
// 			}
// 		}
// 		return $ret;
// 	}
	
	/**
	 * 获得子版块联动/获得指定版块的层次
	 */
	public function get_modules(){
		$this->_need_ajax_post();
		$modules=init_modules_cache();
		switch ($this->oper){
// 			case 'hierarchy':{
// 				$id=I('post.id',0,'int');
// 				$modulesArray=$modules['array'];
// 				$ret=$this->getHierarchyModule($modulesArray, $id);
// 				print_r($ret);
// 			}
			default:{
				$pid=I('post.mid',0,'int');
				$level=I('post.level',1,'int');
				$modulesArray=$modules['array'];
				$ret=array();
				foreach ($modulesArray as $m) {
					//--非叶子版块和有特殊链接地址的版块不可增加文章--
					if($m['module_pid']==$pid&&$m['module_level']==$level+1&&$m['contents']==1){
						array_push($ret,$m);
					}
				}
				$this->ajaxReturn($ret);
			}
		}
	}
	
	/**
	 * 发布帖子
	 */
	public function post_new(){
		if($this->_need_ajax_post(false)){
			$data=array();
			$data['mid']=I('post.module');
			$data['uid']=$this->aid;
			$data['title']=I('post.title');
			$imgs=array();
			$data['introduction']=get_introduction($_POST['content'],DEFAULT_INTRODUCTION_COUNT,$imgs);
			if(!empty($imgs[1][0])) $data['img']=$imgs[1][0];
			$data['dateline']=NOW_TIME;
			if(hasnull($data)){
				$this->error(L('CANT_EMPTY'));
			}else{
				$id=M('Post')->add($data);
				$data['content']=I('post.content');
				if($id){
					$this->success(L('POST_SUCCESS'));
				}else{
					$this->error(L('POST_FAILURE'));
				}
			}
		}else{
			$this->_need_add();
			$this->before_post();
			
			layout(false);
			$this->display();
		}
	}
	
	/**
	 * 帖子列表
	 */
	public function post_list(){
		if($this->_need_ajax_post(false)){
			$this->dao=M('Post');
			$where=array();
			//支持filterToolbar
			$options=array(
					array('field'=>'id','op'=>'eq'),
					array('field'=>'mid','op'=>'eq','ignore'=>0),
					array('field'=>'uid','op'=>'eq','ignore'=>0),
					array('field'=>'title','op'=>'like'),
					array('field'=>'dateline','op'=>'eq'),
					array('field'=>'comments','op'=>'ge'),
					array('field'=>'views','op'=>'ge')
			);
			$where=$this->filterToolbar($options,$_POST);
			echo $this->select(array('condition'=>$where));
		}else{
			$this->before_post();
			$this->_grid_display(I('get.layout'));
		}
	}
	/**
	 * 帖子列表操作
	 */
	public function post_list_oper(){
		$this->_need_ajax_post();
		$this->dao=M('Post');
		$this->data['title']=I('post.title');
// 		$this->data['mid']=I('post.mid');
		$this->data['top']=I('post.top');
		$this->data['dateline']=strtotime(I('post.dateline'));
		$this->oper();
	}
	
	private function before_post(){
		$module=D('Module')->get_key_tuple();
		if(empty($module)){
			exit(L('SET_MODULE'));
		}
		$this->assign('module',$module);
	}
	
	/**
	 * 编辑帖子
	 */
	public function post_edit(){
		if($this->_need_ajax_post(false)){
			$data=array();
			$id=I('post.tid',null,'int');
			if(!$id){
				$this->error(L('UPDATE_FAILURE'));
				return;
			}
			$where=array('id'=>$id);
			$mid=I('post.module',null,'int');
			if($mid) $data['mid']=$mid;
			$data['title']=I('post.title');
			$data['introduction']=get_introduction($_POST['content'],DEFAULT_INTRODUCTION_COUNT, $imgs);
			if(!empty($imgs[1][0])) $data['img']=$imgs[1][0];
			if(hasnull($data)){
				$this->error(L('CANT_EMPTY'));
			}else{
				$data['content']=I('post.content');
				$id=M('Post')->where($where)->save($data);
				if($id){
					$this->success(L('UPDATE_SUCCESS'));
				}else{
					$this->error(L('UPDATE_FAILURE'));
				}
			}
		}else{
			$this->_need_edit();
			//帖子编号
			$tid=I('get.tid');
			if(!empty($tid)){
				$post=M('Post')->getById($tid);
				if(!$post){
					exit(L('NOT_EXISIT'));
				}else{
					$post['content']=htmlspecialchars_decode($post['content']);
					$this->assign('post',$post);
					$currentModule=M('Module')->where(array('id'=>$post['mid']))->find();
					$this->assign('current_module',$currentModule);
				}
			}
			$this->before_post();
			
			layout(false);
			$this->display();
		}
	}
	/**
	 * ------------------------------------------
	 * 语言包相关
	 * ------------------------------------------
	 */
	/**
	 * 语言包
	 */
	public function word(){
		if($this->_need_ajax_post(false)){
			$this->dao=M('Word');
			echo $this->select();
		}else{
			$this->_grid_display();
		}
	}
	/**
	 * 语言包操作
	 */
	public function word_oper(){
		$this->_need_ajax_post();
		$this->dao=M('Word');
		$this->data['name']=I('post.name');
		$this->data['lang']=I('post.lang');
		//$this->data['value']=I('post.value');
		$this->data['value']=$_POST['value'];
		$callback=array('function'=>'init_lang','params'=>true);
		$this->oper(array('success'=>$callback));
	}
	
	/**
	 * ------------------------------------------
	 * 幻灯相关
	 * ------------------------------------------
	 */
	/**
	 * 幻灯列表
	 */
	public function banner_list(){
		if($this->_need_ajax_post(false)){
			$where=array();
			//支持filterToolbar
			$options=array(
					array('field'=>'belong','op'=>'like'),
					array('field'=>'show','op'=>'eq','ignore'=>-1),
			);
			$where=$this->filterToolbar($options,$_POST);
			$this->dao=M('Banner');
			echo $this->select(array('condition'=>$where));
		}else{
			$this->_grid_display();
		}
	}
	/**
	 * 幻灯操作
	 */
	public function banner_list_oper(){
		$this->_need_ajax_post();
		$this->dao=M('Banner');
		$this->data['url']=I('post.url');
		$this->data['title']=I('post.title');
		$this->data['link']=I('post.link');
		$this->data['show']=I('post.show');
		$this->data['position']=I('post.position');
		$this->data['belong']=I('post.belong','index');
		if(empty($this->data['belong'])){
			$this->data['belong']=DEFAULT_BANNER_BELONG;
		}
		$this->oper();
	}
	/**
	 * 新增幻灯
	 */
	public function banner_new(){
		$this->_need_add();
		
		layout(false);
		$this->display();
	}
	
	/**
	 * ---------------------------------------------
	 * 图片相关
	 * ---------------------------------------------
	 */
	/**
	 * 相册列表
	 */
	public function album(){
		if($this->_need_ajax_post(false)){
			$this->dao=M('Album');
			echo $this->select();
		}else{
			$this->_grid_display();
		}
	}
	/**
	 * 相册列表操作
	 */
	public function album_oper(){
		$this->_need_ajax_post();
		$this->dao=M('Album');
		$this->data['name']=I('post.name');
		$this->data['description']=I('post.description');
		$this->data['position']=I('post.position',0,'int');
		$this->data['official']=I('post.official',0,'int');
		$this->data['common']=I('post.common',0,'int');
		$this->data['seen']=I('post.seen',0,'int');
		$this->data['counts']=I('post.counts',0,'int');
		if('add'==$this->oper){
			$this->data['dateline']=NOW_TIME;
		}
		$this->data['cover']=I('post.cover',0,'int');
		$this->oper();
	}
	
	/**
	 * 图片列表
	 */
	public function img_list(){
		if($this->_need_ajax_post(false)){
			$this->dao=M('Img');
			$ret=$this->select(array('retJson'=>false));
			$rows=$ret['rows'];
			foreach ($rows as $i=>$row){
				$row['big']=L('WEB_ROOT').UPLOAD_DIR.$row['path'].'b_'.$row['filename'];
				$ret['rows'][$i]=$row;
			}
			echo $this->ajaxReturn($ret);
		}else{
			$this->_grid_display();
		}
	}
	
	/**
	 * 图片列表操作
	 */
	public function img_list_oper(){
		$this->_need_ajax_post();
		$this->dao=M('Img');
		$this->data['name']=I('post.name');
		$this->data['description']=I('post.description');
		$this->data['aid']=I('post.aid',0,'int');
		$this->oper();
	}
	
	/**
	 * 图片批量上传
	 */
	public function img_upload(){
		$this->_need_add();
		
		layout(false);
		$this->display();
	}
	
	/**
	 * 友情链接
	 */
	public function link(){
		if($this->_need_ajax_post(false)){
			$this->dao=M('Link');
			echo $this->select();
		}else{
			$this->_grid_display();
		}
	}
	/**
	 * 友情链接操作
	 */
	public function link_oper(){
		$this->_need_ajax_post();
		$this->dao=M('Link');
		$this->data['url']=I('post.url');
		$this->data['title']=I('post.title');
		$this->data['link']=I('post.link');
		$this->data['show']=I('post.show');
		$this->data['position']=I('post.position',0,'int');
		$callback=array('function'=>'get_caches','params'=>array('link',true));
		$this->oper(array('success'=>$callback));
	}
}