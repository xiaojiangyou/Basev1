<?php
namespace Admin\Controller;

use \DOMDocument;

/**
 * 菜单管理表格实现子类
 */
class MenuController extends JqGridController{
	private $doc;//dom对象
	private $path;//文件地址
	private $rows;//所有的row
	private $root;//根目录rows
	public function __construct() {
		parent::__construct ();
		$this->doc=new DOMDocument('1.0','utf-8');//创建DOM对象
		$this->doc->formatOutput=true;
		$this->doc->preserveWhiteSpace=false;//允许出现空格
		$this->path=XML.'/menu.xml';//文件路径
		$this->doc->load($this->path);	//读取xml
		$this->rows=$this->doc->getElementsByTagName('row');//获得所有标签是row的所有标签内容
		$this->root=$this->doc->getElementsByTagName('rows')->item(0);
	}
	/**
	 *  菜单管理
	 */
	public function menu(){
		$this->_grid_display();
	}
	/**
	 * 菜单操作入口函数
	 */
	public function menu_oper(){
		$ret=false;
		if(in_array($this->oper,array('edit','add','del'))){
			$oper=$this->oper;
			if($oper=='del') $oper='delete';
			$ret=$this->$oper();
		}
		if(!$ret){
			$this->error(L('UPDATE_FAILURE'));
		}else{
			$this->doc->save($this->path);
			$ret=$this->sortMenu($this->path);
			init_items(true);
			init_menu(true);
			$this->success(L('UPDATE_SUCCESS'));
		}
	}
	/**
	 * ---------------------------------------------
	 * 页面操作
	 * ---------------------------------------------
	 */
	/**
	 * 编辑一个节点
	 */
	protected final function edit(&$condition=array(),&$data=array()){
		$post=I('post.');
		//菜单编号
		$id=$post['id'];
		$newid=$post['no'];
		$url=$post['url']=='无'?null:$post['url'];
		$cells=array($newid,$post['name'],$url,$post['tip'],$post['power'],$post['level'],$post['father'],$post['isleaf'],$post['expand']);
		//修改编号
		if($id!=$newid){
			foreach( $this->rows as $row )
			{
				if($row->getAttribute('id')==$newid){
					$this->error(L('ID_EXISIT'));
					return false;
				}
				else continue;
			}
		}
		foreach( $this->rows as $row )
		{
			$item=$row->getElementsByTagName('cell');
			if($row->getAttribute('id')==$id)//获得row的id属性
			{
				for($i=0;$i<count($cells);$i++){
					$item->item($i)->nodeValue=$cells[$i];
				}
				$row->setAttribute('id',$newid);
				break;
			}
		}
		return true;
	}
	/**
	 * 增加一个节点
	 */
	protected final function add(&$data=array()){
		$post=I('post.');
		$url=$post['url']=='无'?null:$post['url'];
		$id=$post['no'];
		foreach( $this->rows as $row )
		{
			if($row->getAttribute('id')==$id){
				$this->error(L('ID_EXISIT'));
				return false;
			}
			else continue;
		}
		$cells=array($id,$post['name'],$url,$post['tip'],$post['power'],$post['level'],$post['father'],$post['isleaf'],$post['expand']);
		//创建一行
		$row=$this->doc->createElement('row');
		//设置id属性，默认输入的编号即为id
		$row->setAttribute('id',$id);
		for($i=0;$i<count($cells);$i++){
			$cell=$this->doc->createElement('cell');
			$cell->appendChild($this->doc->createTextNode($cells[$i]));
			$row->appendChild($cell);
		}
		$this->root->appendChild($row);
		return true;
	}
	/**
	 * 删除一个节点
	 */
	protected final function delete(&$condition=array()){
		$id=I('post.id');
		foreach( $this->rows as $row )
		{
			if($row->getAttribute('id')==$id)//获得row的id属性
			{
				$this->root->removeChild($row);
				break;
			}else continue;
		}
		return true;
	}
	/**
	 * ---------------------------------------------
	 * 其他操作
	 * ---------------------------------------------
	 */
	/**
	 * 将当前xml文件排序后保存到相应地址的xml文件中
	 * @param string $outpath 保存路径地址
	 * @param string $power 用户可拥有的权限，为空表示有所有权限（默认）
	 */
	private function sortMenu($outpath,$power=array()) {
		$output = new DOMDocument ( '1.0', 'utf-8' );
		$output->formatOutput = true;
		$output->preserveWhiteSpace = false; // 允许出现空格
		$root = $output->createElement ( 'rows' );
		$output->appendChild ( $root );
	
		$root->appendChild ( $output->createElement ( 'page', 1 ) );//当前页
		$root->appendChild ( $output->createElement ( 'total', 1 ) );//总页数
		$root->appendChild ( $output->createElement ( 'records', 0 ) );
	
		//获得用户的所有权限
		$powers=$this->powers();
	
		foreach ( $this->rows as $row ) {
			$rowid=$row->getAttribute ( 'id' );
			if(!empty($power)){
				//无此权限
				if(!$this->checkPower($rowid, $powers, $power)){
					continue;
				}
			}
			$outRows = $output->getElementsByTagName ( 'row' );
			if ($outRows->length == 0) {
				$root->appendChild ( $output->importNode ( $row, true ) );
			} else {
				foreach ( $outRows as $j => $outrow ) {
					if ($this->compare( $rowid, $outrow->getAttribute ( 'id' ))) {
						$root->insertBefore ( $output->importNode ( $row, true ), $outrow );
						break;
					} else  {
						if ($j == $outRows->length - 1) {
							$root->appendChild ( $output->importNode ( $row, true ) );
							break;
						} else {
							continue;
						}
					}
				}
			}
		}
		$output->getElementsByTagName('records')->item(0)->nodeValue=$output->getElementsByTagName('row')->length;
		$output->save ( $outpath );
	}
	/**
	 * 菜单编号比较i和j的大小，i<j返回true
	 * @example 01<02<0201<020101<0202<03
	 * @param int $i 第一个字符串
	 * @param int $j 第二个字符串
	 * @return boolean
	 */
	private function compare($i,$j){
		$ilen=strlen($i);
		$jlen=strlen($j);
		if ($ilen!=$jlen){//两者的长度不同，根节点长度为2，下层节点的长度为4，依次类推
			if($ilen>$jlen){//i的长度大于j
				$tmp=str_split($i,$jlen);
				$i=$tmp[0];
			}else{
				$tmp=str_split($j,$ilen);
				$j=$tmp[0];
			}
		}
		$i=intval($i);
		$j=intval($j);
		if(($i<$j)||($i==$j&&$ilen<$jlen)){
			return true;
		}
		return false;
	}
	/**
	 * 判断用户是否有权限
	 * @param int $rowid row的id属性
	 * @param array $powers 用户所有权限设置表
	 * @param array $power 用户权限
	 */
	private function checkPower($rowid,&$powers,$power){
		if(!empty($powers[$rowid])){//此为可设权限项
			if($power[$rowid]['has_power']==1){
				return true;
			}
		}else{
			//每次层占两位，获得上层的权限
			$pid=substr($rowid,0,strlen($rowid)-2);
			if($powers[$pid]&&$power[$pid]['has_power']==1){
				return true;
			}
		}
		return false;
	}
	/**
	 * 获得所有可以管理的菜单项，包含需要权限的项
	 * @return array $power 菜单权限数组 array(菜单id=>array(节点名称 ，节点说明，需要权限，节点层次))
	 */
	public function powers(){
		$power=array();
		foreach ($this->rows as $row){
			$item=$row->getElementsByTagName('cell');
			//需要权限|单节点|不是最终叶节点 进行配置
			if($item->item(4)->nodeValue=='true'||$item->item(5)->nodeValue==1||$item->item(7)->nodeValue=='false'){
				$power[$item->item(0)->nodeValue]=array('name'=>$item->item(1)->nodeValue,'tips'=>$item->item(3)->nodeValue,'need_power'=>$item->item(4)->nodeValue,'level'=>$item->item(5)->nodeValue);
			}else{
				continue;
			}
		}
		return $power;
	}
	/**
	 * 获得所有叶子节点（及所有的管理页面）
	 * @return array $power 权限数组 array(权限id=>array(是否有权限，节点说明，需要权限，节点层次))
	 */
	public function items(){
		$power=array();
		foreach ($this->rows as $row){
			$item=$row->getElementsByTagName('cell');
			if($item->item(7)->nodeValue=='true'){
				$power[$item->item(0)->nodeValue]=array('id'=>$item->item(0)->nodeValue,'name'=>$item->item(1)->nodeValue,'tpl'=>$item->item(2)->nodeValue);
			}else{
				continue;
			}
		}
		return $power;
	}
	/**
	 * 重置管理组对应的xml文件，在修改权限的时候触发
	 * @param int $group_id 管理组编号
	 */
	public function reset_xml($group_id=0){
		//权限表
		$dao=M('Power');
		if($group_id!=0) {
			$where['group_id']=$group_id;
			$dao->where($where);
		}
		$haspower=$dao->select();
		if($group_id!=0){
			$power=array();
			if(!empty($haspower)){
				foreach ($haspower as $p){
					$power[$p['power_id']]=$p;
				}
				$this->sortMenu(XML.'/menu_'.$group_id.'.xml',$power);
			}
		}else{
			$resetPower=array();
			if(!empty($haspower)){
				foreach ($haspower as $p){
					$resetPower[$p['group_id']][$p['power_id']]=$p;
				}
				foreach ($resetPower as $gid=>$power){
					if(!empty($power)){
						$this->sortMenu(XML.'/menu_'.$gid.'.xml',$power);
					}
				}
			}
		}
	}
}