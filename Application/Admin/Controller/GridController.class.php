<?php
namespace Admin\Controller;

use Think\Model;

/**
 * 后台表格类操作抽象类
 */
abstract class GridController extends BaseController{
	/**
	 * 操作的数据表
	 * @var Model
	 */
	protected $dao;
	/**
	 * 是否为查询
	 * @var boolean
	 */
	protected $search=false;
	/**
	 * 
	 * @var array('page'=>,'rows'=>,(前2项必须)'order'=>(可选)) 
	 */
	protected $options=array();
	/**
	 * 抽象方法：设置参数的获取规则
	 */
	protected abstract function set_options();
	/**
	 * 构造函数
	 */
	public function __construct(){
		parent::__construct();
	}
	/**
	 * 添加新纪录
	 * @param array $data 新增的数据
	 * @return boolean
	 */
	protected function add(&$data){
		if(!empty($data))
			if($this->dao->add($data)) return true;
		return false;
	}
	/**
	 * 删除纪录
	 * @param array $condition 删除条件
	 * @return boolean
	 */
	protected function delete(&$condition){
		if(!empty($condition)) 
			if($this->dao->where($condition)->delete()) return true;
		return false;
	}
	/**
	 * 编辑纪录
	 * @param array $condition 编辑条件
	 * @param array $data 编辑数据 
	 * @return boolean
	 */
	protected function edit(&$condition,&$data){
		if(!empty($condition)&&!empty($data))
			if($this->dao->where($condition)->save($data)!==false) return true;
		return false;
	}
	/**
	 * 抽象方法：获取搜索条件
	 */
	protected abstract function get_condition();
	/**
	 * 查询数据
	 * @param array $options 指定扩展条件 array('condition'=>用户查询条件,'fields'=>查询的字段,'order'=>排序方式,,'retJson'=>返回json格式数据,...)
	 * @return array('counts'=>,'pages'=>,'page'=>,'result'=>array())
	 */
	protected function select($options){
		//设置默认参数的获取规则
		$this->set_options();
		
		if(!$this->dao) return false;
		//获得用户配置的搜索条件
		$condition=$this->get_condition();
		//用户指定查询条件
		if(!empty($options['condition'])) $this->dao->where($options['condition']);
		if(!empty($condition)) $this->dao->where($condition);
		
		//获得纪录数
		$results['counts']=$this->dao->count();
		
		//查询条件重置，为查询准备
		if(!empty($options['condition'])) $this->dao->where($options['condition']);
		if(!empty($condition)) $this->dao->where($condition);
		
		//查询到的页数
		if($this->options['rows']>0){
			$results['pages']=ceil($results['counts']/$this->options['rows']);
		}else{//默认为一页
			$results['pages']=1;
		}
		if(!isset($this->options['page'])||!isset($this->options['rows'])){
			return false;
		}
		if($results['pages']>0&&$this->options['rows']>0){
			$this->dao->limit((($this->options['page']-1)*$this->options['rows']).','.$this->options['rows']);
		}
		//当前页码
		$results['page']=$this->options['page'];
		
		//由表格插件前台传递默认的排序方式
		if(!empty($this->options['order'])) $order=$this->options['order'];
		//用户指定的排序方式代替默认的排序方式
		if(!empty($options['order'])) $order=$options['order'];
		if(!empty($order)) $this->dao->order($order);
		
		if(isset($options['fields'])) $this->dao->field($options['fields']);
		
		$results['result']=$this->dao->select();
		return $results;
	}
}