<?php
namespace Admin\Controller;

use Common\Controller\CommonController;

/**
 * 后台管理抽象基类
 */
abstract class BaseController extends CommonController{
	/**
	 * 当前登陆管理员的编号
	 * @var int
	 */
	protected $aid;
	/**
	 * 构造函数：引入后台函数并初始化管理员编号成员变量
	 */
	public function __construct(){
		parent::__construct();
		include_once MODULE_PATH.'Common/function_admin.php';
		if(!isset($this->aid)) $this->aid=session('aid');
	}
	/**
	 * 检查管理员是否登录
	 */
	protected function _islogin(){
		if(empty($this->aid)) return false;
		else return true;
	}
	
	/**
	 * 需要登陆
	 */
	protected function _need_login(){
		if(!$this->_islogin()){
			$this->error(L('NEED_LOGIN'));
			return false;
		}
		return true;
	}
	/**
	 * ----------------------------------------
	 * 操作权限相关
	 * ----------------------------------------
	 */
	/**
	 * 输出操作权限
	 */
	protected function _operation_power(){
		$item=$this->_item();
		//输出操作权限字符串应用于表格相应显示
		$this->assign('edit',$item['edit']==1?'true':'false');
		$this->assign('add',$item['add']==1?'true':'false');
		$this->assign('del',$item['del']==1?'true':'false');
		$this->assign('search','false');
		//布尔型操作权限
		$this->assign('editBoolen',$item['edit']);
		$this->assign('addBoolean',$item['add']);
		$this->assign('delBoolean',$item['del']);
	}
	/**
	 * 获得当前管理项的操作权限
	 */
	protected function _item(){
		$items=session('items');
		$menu=init_menu();
		return $items[$menu[CONTROLLER_NAME][ACTION_NAME]];
	}
	/**
	 * 需要编辑操作权限
	 */
	protected function _need_edit(){
		$item=$this->_item();
		if(!$item['edit']){
			exit(L('NO_POWER_TIP'));
		}
	}
	/**
	 * 需要新增操作权限
	 */
	protected function _need_add(){
		$item=$this->_item();
		if(!$item['add']){
			exit(L('NO_POWER_TIP'));
		}
	}
	
	/**
	 * 通用获得指定表的键值对
	 */
	public function key_value(){
		if(in_array($this->oper,array('module','admin','group','album','enterprise'))){
			$dao=M($this->oper);
			$where=array();
			switch($this->oper){
				case "module":
					//需要是最终子节点
// 					$where['contents']=1;
// 					$where['url']='';
				default:{
					$id=I('post.fid',null,'int');
					if($id){
						$where['id']=$id;
					}
					if(!empty($where)){
						$dao->where($where);
					}
					$this->ajaxReturn($this->_get_key_value($dao));
				}
			}
		}else{
			return;
		}
	}
}