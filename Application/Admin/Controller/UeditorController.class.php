<?php
namespace Admin\Controller;

use Util\Ueditor;

/**
 * Ueditor编辑器控制器
 */
class UeditorController extends BaseController{
	private $ueditor;
	public function __construct(){
		parent::__construct();
		$this->ueditor=new Ueditor();
	}
	/**
	 * 获得最近上传的图片列表
	 */
	public function get_image(){
		echo $this->ueditor->get_image($this->aid);;
	}
	/**
	 * 解析视频
	 */
	public function parse_video(){
	}
	/**
	 * 上传文件
	 */
	public function upload_file(){
		if(!$this->_islogin()){
			//exit($this->ajaxReturn(array('state'=>L('NO_POWER'))));
			echo json_encode(array('state'=>L('NO_POWER')));
			return;
		}
		$type=I('get.type','File');
		$this->ueditor->type=$type;
		$this->ueditor->file=$_FILES['upfile'];
		//上传文件
		$info=$this->ueditor->upload_file();
		$data=array();
		if(!is_array($info)){
			//exit($this->ajaxReturn(array('state'=>$info)));
			echo json_encode(array('state'=>$info));
			return;
		}
		switch ($type){
			case 'Image':{
				$info['description']=I('post.title');
				$info['uid']=$this->aid; // 上传者
				$id=D('Img')->add_upload($info);
				if($id){
					$thumbs=$this->ueditor->auto_thumb($info['savepath'], $info['savename']);
					$data['url']=$thumbs['big'];
					$data['title']=$info['description'];
					$data['original']=$info['name'];
					$data['state']='SUCCESS';
				}else{
					$data['state']=L('UPLOAD_FAILURE');
				}
				break;
			}
			default:{
				$info['uid']=$this->aid; // 上传者
				$id=D('File')->add_upload($info);
				if($id){
					$data['url']=UPLOAD_DIR.$info['savepath'].$info['savename'];
					$data['fileType']=$info['ext'];
					$data['original']=$info['name'];
					$data['state']='SUCCESS';
				}
				else{
					$data['state']=L('UPLOAD_FAILURE');
				}
			}
		}
		//此处为了ajaxUploader插件不能设置header为json
		//exit($this->ajaxReturn($data));
		echo json_encode($data);
	}
}

