<?php
namespace Admin\Controller;

/** * 后台入口控制器
 */
class IndexController extends BaseController {
	public function __construct(){
		parent::__construct();
		init_admin();
	}
	/**
	 * ------------------------------------------------
	 * 模板文件说明：
	 * (1)命名规则：
	 *    -语言包提示以"模版名称"_TIP命名
	 * (2)新增原则：
	 *    -常规表格+提示可继承使用默认模版grid.html
	 *     表格类格式参考此模版布局
	 *    -自定义的模版文件格式可参考power.html
	 * (3)刷新原则：
	 * 	  -默认以load方式打开页面，js必须重新加载
	 *    -使用默认模版的那些方法不需要改动
	 * ------------------------------------------------
	 */
	/**
	 * 管理中心入口页
	 */
	public function index(){
		if(!$this->_islogin()){
			//路由设置
			$this->redirect('/admin/login');
		}
		$admin=session('admin');
		$menu='/menu.xml';
		if(SYSTEM_GROUP!=$admin['group_id']){
			$menu='/menu_'.$admin['group_id'].'.xml';
		}
		$this->assign('menu',$menu);
		$this->display();
	}
	/**
	 * 登录
	 */
	public function login(){
		if($this->_islogin()){
			$this->redirect('/admin/home');
		}
		if($this->_need_ajax_post(false)){
			$dao=D('Admin');
			if($dao->check(I('username',null),I('password',null))){
				init_admin();
				$this->success(L('LOGIN_SUCCESS'));
			}else{
				$this->error($dao->getError());
			}
		}else{
			$this->display();
		}
	}
	/**
	 * 登出
	 */
	public function logout(){
		session_destroy();
		$this->redirect('/admin/login');
	}
}