<?php
/* 扩展配置项 */
// 系统管理员 ManageController
define('SYSTEM_GROUP',1);
// DOM读XML的路径有所差别  MenuController
define('XML','Public/Data/Xml');
// 表格每页默认显示的记录数目 JqGridController
define('DEFAULT_ROWS',20);
//上传图片地址 UeditorController
define('UPLOAD_DIR','./Uploads/');
?>