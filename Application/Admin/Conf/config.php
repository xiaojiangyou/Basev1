<?php
return array(
    // 加载扩展配置文件
    'LOAD_EXT_CONFIG' => 'admin',
    
    // 模板布局
    'LAYOUT_ON' => true,
    'LAYOUT_NAME' => 'layout',
    
    // 路由的相关配置
    // 路由开启
    'URL_ROUTER_ON' => true,
    'URL_ROUTE_RULES' => array(
        // 后台页面
        'home' => array(
            'Admin/Index/index'
        ),
        'login' => array(
            'Admin/Index/login'
        ),
        'logout' => array(
            'Admin/Index/logout'
        )
    )
);
?>