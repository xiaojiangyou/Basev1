/**
 * 全局使用的公共函数
 */
/**
 * 检查变量是否已经定义
 * @param Var 变量
 * @returns {Boolean}
 */
function isDefined(Var){
	if(typeof(Var)=="undefined") return false;
	else return true;
}
/**
 * 检查变量是否为空或未定义
 * @param Var 变量
 * @returns {Boolean}
 */
function isEmpty(Var){
	if(!isDefined(Var)||Var==null||Var=="") return true;
	return false;
}
/**
 * 字符串转化为整型
 * @param str 字符串形式的整型数字
 * @param Default 默认为0
 * @returns {Integer} 整型数字
 */
function strToInt(str,Default){
	if(typeof(Default)!="number") Default=0;
	var num=Default;
	try{
		num=parseInt(str);
	}catch(e){
		num=Default;
	}
	if(num==NaN)num=Default;
	return num;
}
/**
 * 通过时间戳获取JS的日期对象，不考虑JS与PHP的时间戳差异
 * @param {Integer} 时间戳
 */
function getTimeStrByStmp(nS) {
	nS=parseInt(nS);
	if(nS<10000000000)nS=nS*1000;
	var d=new Date(nS);
	return d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate()+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
}

/**
 * 删除左右两端的空格
 * @param str
 * @returns
 */
function trim(str){
    return str.replace(/(^\s*)|(\s*$)/g, "");
}

/**
 * 显示
 * @param obj
 * @param content
 * @param alignX
 * @param alignY
 */
function showTip(obj,content,alignX,alignY){
	if(!isDefined(alignX)||!isDefined(alignY)){
		alignX='right';
		alignY='center';
	}
	obj.poshytip('destroy');
	obj.poshytip({
		content:content,
		alignTo:'target',
		alignX:alignX,
		alignY:alignY,
		showOn:'none'
	});
	obj.poshytip('show');
	obj.focus();
}
/**
 * 隐藏
 * @param obj
 */
function hideTip(obj){
	obj.poshytip('hide');
}


function intText(obj){
	obj.keydown(function(event){
		var keyCode = event.which;
        if (keyCode==45 || keyCode == 46 || (keyCode >= 48 && keyCode <=57) || keyCode == 8|| keyCode==9 || keyCode>=96 && keyCode<=105)//8鏄垹闄ら敭
          	return true;
        else
        	return false;
    }).focus(function() {
        this.style.imeMode='disabled';
    }).keyup(function(){
    	$(this).val($(this).val().replace(/[^0-9]+-/,''));
    }).bind("paste",function(){
    	return false;
    }).bind("contextmenu",function(e){
    	return false;
	});
}
/**
 * --------------------------------------------------
 * 弹出层函数，调用artDialog 5.0.3
 * --------------------------------------------------
 */
/**
 * 弹出ajax页面
 * @param url 获取页面的地址
 * @param param ajax参数
 * @param title 弹出页面的标题
 * @param zindex 弹出页面的zindex
 * @returns dialog对象
 */
function artPage(url,param,title,zindex){
	if(!isDefined(title)) title="";
	if(!isDefined(param)) param=[];
	if(!isDefined(zindex)) zindex=300;
	var dialog=art.dialog({
		id:"art_page"+Math.random(),
		title:title,
		fixed:true,
		lock:true,
		zIndex:zindex
	});
	if($("#art_page_div").length==0){
		$("#wrap").append("<div id='art_page_div' class='hidden'></div>");
	}
	$("#art_page_div").load(url,param,function(){
		dialog.content(document.getElementById('art_page_div'));
	});
	return dialog;
}
/**
 * 警告弹出层
 * @param warning 弹出消息
 * @param callback 回调函数
 * @returns dialog对象
 */
function artWarning(warning,callback){
	var dialog=artBase(warning,"warning");
	dialog.title("警告");
	dialog.button({
		id:'close',
	    value:'',
	    callback:callback
	});
	return dialog;
}
/**
 * 操作成功弹出层
 * @param success 弹出消息
 * @param time 显示弹出层的毫秒，0表示常显示
 * @param callback 点击确定触发的函数
 * @returns dialog对象
 */
function artSuccess(success,time,callback){
	if(isEmpty(time)) time=0;
	var dialog=artBase(success,"succeed");
	dialog.time(time);
	dialog.button({
		id:'close',
	    value:'确定',
	    callback:callback
	});
	return dialog;
}
/**
 * 错误弹出层
 * @param error 弹出消息
 * @param callback 点击关闭触发的函数
 * @returns dialog对象
 */
function artError(error,callback){
	var dialog=artBase(error,"error");
	dialog.title("错误");
	dialog.button({
		id:'close',
	    value:'关闭',
	    callback:callback
	});
	return dialog;
}
/**
 * 自定标题弹出层
 * @param success 弹出消息
 * @param title 弹出页面的标题
 * @param callback 回调函数
 * @returns dialog对象
 */
function artDialog(success,title,callback){
	var dialog=artBase(success);
	if(!isEmpty(title)){
		dialog.title(title);
	}
	dialog.button({
		id:'close',
	    value:'确定',
	    callback:callback
	});
	return dialog;
}
/**
 * 提示弹出层，弹出层标题隐藏
 * @param tip 弹出消息
 * @param time 显示时间
 * @param callback 回调函数
 */
function artTip(tip,time,callback){
	var dialog=artBase(tip,"succeed",function(){
		$(".d-header").css("display","none");
	},function(){
		return false;
	});
	dialog.lock();
	if(isEmpty(time)) time=0;
	dialog.time(time);
	if(time>0){
		setInterval(callback,time/1.5);//避免延迟
	}
}
/**
 * 弹出空白的弹出层
 * @returns
 */
function artEmpty(page){
	var dialog=artBase(page,"",function(){
		$(".d-header").css("display","none");
	});
	return dialog;
}
/**
 * 加载中弹出层
 * @param tip 弹出消息
 */
function artLoading(tip){
	var dialog=artBase(tip,"loading",function(){
		$(".d-header").css("display","none");
	},function(){
		return false;
	});
	dialog.lock();
}
/**
 * 确认弹出层
 * @param confirm 弹出层显示内容
 * @param success 成功回调函数
 * @param error 失败回调函数
 * @param title 弹出层标题
 * @returns dialog对象
 */
function artConfirm(confirm,success,error,title){
	var dialog=artBase(confirm,"question");
	if(!isEmpty(title)) dialog.title(title);
	if(isEmpty(success)){
		success=function(){
			return true;
		};
	}
	if(isEmpty(error)){
		error=function(){
			return true;
		};
	}
	dialog.button({
		id:"success",
		value:"确认",
		callback:success
	},{
		id:"cancel",
		value:"取消",
		callback:error
	});
	return dialog;
}
/**
 * 弹出层基本配置
 * @param string icon 图标名称
 * @param string info 提示信息
 * @param function initialize 初始化函数
 * @param function close 关闭时函数
 */
function artBase(info,icon,initialize,close){
	var content="";
	var padding="20px 35px";
	if(!isEmpty(icon)){
		if(icon=="loading"){
			icon+=".gif";
			padding="5px 10px";
		}else{
			icon+=".png";
		}
		content="<table height='42px;'><tr><td><img src='"+IMG+"/icons/"+icon+"'></td>";
	};
	content+="<td style='margin-left:10px;'>"+info+"</td></tr></table>";
	var dialog=art.dialog({
		id:"dialog"+Math.random(),
		title:"提示",
		content:content,
		fixed:true,
		lock:true,
		padding:padding,
		initialize:initialize,
		beforeunload:close
	});
	return dialog;
}