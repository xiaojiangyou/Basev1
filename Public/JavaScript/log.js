//console日志记录函数
var debuging=true;//是否正在调试
function log(txt){
	if(debuging!=true)return;
	try{
		console.log(txt);
	}catch(e){}
}
function debug(txt){
	if(debuging!=true)return;
	try{
		console.debug(txt);
	}catch(e){}
}
function info(txt){
	if(debuging!=true)return;
	try{
		console.info(txt);
	}catch(e){}
}
function warn(txt){
	if(debuging!=true)return;
	try{
		console.warn(txt);
	}catch(e){}
}
function error(txt){
	if(debuging!=true)return;
	try{
		console.error(txt);
	}catch(e){}
}
/**
 * 显示obj的所有属性和方法
 * @param obj
 */
function dir(obj){
	if(debuging!=true)return;
	try{
		console.dir(obj);
	}catch(e){}
}
/**
 * 显示网页的某个节点（node）所包含的html/xml代码
 * @param node
 */
function dirxml(node){
	if(debuging!=true)return;
	try{
		console.dirxml(obj);
	}catch(e){}
}
/**
 * 判断一个表达式或变量是否为真。如果结果为否，则在控制台输出一条相应信息
 * @param ValOrExp
 */
function assert(ValOrExp){
	if(debuging!=true)return;
	try{
		console.assert(ValOrExp);
	}catch(e){}
}
/**
 * 追踪当前函数的调用轨迹
 */
function trace(){
	if(debuging!=true)return;
	try{
		console.trace(ValOrExp);
	}catch(e){}
}
/**
 * 在函数前后调用下面两个函数可以显示函数运行时间
 */
function timeStart(){
	if(debuging!=true)return;
	try{
		console.time();
	}catch(e){}
}
function timeEnd(){
	if(debuging!=true)return;
	try{
		console.timeEnd();
	}catch(e){}
}
/**
 * 在函数前后调用下面两个函数可以显示函数运行效率
 */
function profileStart(){
	if(debuging!=true)return;
	try{
		console.profile();
	}catch(e){}
}
function profileEnd(){
	if(debuging!=true)return;
	try{
		console.profileEnd();
	}catch(e){}
}
/**
 * 将console中的log进行分组，groupStart和groupEnd之间的部分将会被分成一组显示
 */
function groupStart(){
	if(debuging!=true)return;
	try{
		console.group();
	}catch(e){}
}
function groupEnd(){
	if(debuging!=true)return;
	try{
		console.groupEnd();
	}catch(e){}
}