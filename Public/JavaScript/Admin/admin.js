/**
 * --------------------------------------------------
 * Jqgrid相关
 * --------------------------------------------------
 */
/**
 * 修改 jqgrid 增删查提示
 * @param response 响应内容
 * @param postdata
 * @returns {Array}
 */
function process(response,postdata){
	//第一个参数为服务器返回的json数据
	var success=true;
	var json = toJson(response);
	var message=json.info;
	if(1!=json.status){
		success=false;
	}
	return [success,message];
}
/**
 * 后台传回数据转化为 json 数据
 * @param response 内容
 * @returns json 数据
 */
function toJson(response){
	return eval("(" + response.responseText + ")");
}
/**
 * 调用 jqgrid 的错误提示框
 * @param info 提示信息
 */
function gridWarning(info) {
	$.jgrid.info_dialog($.jgrid.errors.errcap,info,$.jgrid.edit.bClose);
}
/**
 * jqgrid 简单添加时间插件
 * @param e 当前dom对象
 */
function initTime(e){
	$(e).datepicker({
		dateFormat:"yy-mm-dd"
	});
}
/**
 * jqgrid 修改时显示的不可修改内容
 * @param value
 * @param options
 * @returns {String}
 */
function staticElement(value, options) {
	var elem="<span>"+value+"</span>";
	return elem;
}
/**
 * jqgrid 修改时获取显示的不可修改内容
 * @param elem
 * @param operation
 * @param value
 * @returns {String}
 */
function staticValue(elem, operation, value) {
	if(operation === "get") {
		return $(elem).text();
	} else if(operation ==="set") {
		$(elem).text(value);
	}
}
/**
 * 如果为空则返回“无”
 * @param v
 * @returns {String}
 */
function nullToStr(v){
	if(!isDefined(v)) return "无";
	else return v;
}
/**
 * 添加不限的选项
 * @param data
 * @returns
 */
function addNoLimit(data){
	data="0:不限;"+data;
	return data;
}
/**
 * 链接格式化，在表格中增加扩展链接方法
 * @param cellvalue
 * @param options
 * @param rowObject
 * @returns {String}
 */
function actionLinksFormatter(cellvalue, options, rowObject){
	var htm="",href="",target="",onclick="",text="",className="",title="";
	var items=options.colModel.formatoptions.items;
	$.each(items,function(i,item){
		href=!isDefined(item.href)?'':item.href;
		text=!isDefined(item.text)?'':item.text;
		className=!isDefined(item.className)?'jqgrid-link-oper':item.className;
		title=!isDefined(item.title)?text:item.title;
		if(!isEmpty(href)){
			target=!isDefined(item.target)?'_blank':"target='"+item.target+"'";
			htm+="<a rowId='"+options.rowId+"' href='"+href+"' "+target+" class='"+className+"' title='"+title+"'>"+text+"</a>";
		}else{
			onclick=!isDefined(item.onclick)?'':"onclick='"+item.onclick+"'";
			htm+="<span rowId='"+options.rowId+"' class='ui-icon jqgird-link-icon "+className+"' "+onclick+" title='"+title+"'>"+text+"</span>";
		}
		
	});
	return htm;
}
/**
 * --------------------------------------------------
 * 获取数据，需要后台支持
 * --------------------------------------------------
 */
/**
 * 获取字符串填入jqgrid的select框
 * @param url 请求地址
 * @param oper 操作类型
 * @param fid 获取的字段编号
 * @returns {String} 
 */
function getJqgridStringData(url,oper,fid){
	var data="";
	var json=getJsonData(url,oper,fid);
	if(!isEmpty(json)){
		data=selValFromJson(json);
	}
	return data;
}
/**
 * 获取键值对json数据
 * @param url 请求地址
 * @param oper 操作类型
 * @param fid 获取的字段编号
 * @returns {Json}
 */
function getJsonData(url,oper,fid){
	var data="";
	if(!isDefined(fid)) fid=0;
	$.ajax({
		   type:"POST",
		   url:url,
		   data:{"oper":oper,"fid":fid},
		   async:false,//多个请求要同步进行
		   dataType:"json",
		   success:function(json){
			   data=json;
		   }
	});
	return data;
}
/**
 * 从 json 数组转化为 jqgrid 的 select 框的 value 值（字符串）格式
 * @param json json格式数据
 * @returns {String}
 */
function selValFromJson(json){
	var val="";
	var value=new Array();
	if(!isEmpty(json)){
		$.each(json,function(i,v){
			value.push(i+":"+v);
		});
		val=value.join(";");
	};
	return val;
}
/**
 * 将 json 数据转化成 option 的 html
 * @param json json格式数据
 * @returns {String}
 */
function optionFromJson(json){
	var options="";
	$.each(json,function(i,v){
		options+="<option value='"+i+"'>"+v+"</option>"
	});
	return options;
}
/**
 * --------------------------------------------------
 * 页面相关
 * --------------------------------------------------
 */
/**
 * 打开tab窗口
 * @param id tab的编号
 * @param caption tab的名称
 * @param url tab访问的后台地址
 * @param callback 回调函数
 * @param remove 是否是关闭该标签
 */
function openTab(id,caption,url,callback,remove){
	if(!isDefined(tabs)) return false;
	if(!isDefined(remove)) remove=false;
	//已经存在此tab
	if($("#tabs_"+id).size()>0){
		if(remove==false){
			tabs.tabs("select","#tabs_"+id);
			return true;
		}else{
			tabs.tabs("remove",id);
		}
	}
	tabs.tabs("add","#tabs_"+id,caption);
	$("#tabs_"+id).addClass("tabs").load(url,function(){
		if(typeof(callback)=="function")callback();
		resizeElements();
	});
}
/**
 * 给左侧 menu 添加 anchor
 * 使得点击某个菜单对应的url之后加上#
 */
function menuAnchor(){
	$("#menu tbody tr[role='row']").each(function(){
		if($("td[aria-describedby='menu_isLeaf']",this).html()=="true"){
			var id=$(this).attr("id");
			var td=$("td[aria-describedby='menu_menu']",this);
			var htm=td.html();
			td.html("<a href='#"+id+"'>"+htm+"</a>");
		}
	});
}
/**
 * 根据当前页面窗口 url 的 # 之后的参数打开 tab 窗口
 */
function openTabFromAnchor(){
	var url=document.location.href;
	var start=url.lastIndexOf('#');
	if(start>0){
		var menuid=url.substr(start+1,url.length-start);//获取url后面#之后的参数
		var menu_row=$("#menu").getRowData(menuid);
		var action=menu_row.action;
		if(!isEmpty(action)){
			openTab(menuid,menu_row.menu,BASE+"/admin/"+action);
		}
	}
}
/**
 * --------------------------------------------------
 * 其他公共函数
 * --------------------------------------------------
 */
/**
 * 时间戳格式化
 * @param cellvalue
 * @param options
 * @param rowObject
 * @returns {String}
 */
function timeStampFormat(cellvalue, options, rowObject){
	cellvalue=strToInt(cellvalue,0);
	if(cellvalue<=0)return '';
	return getDateStrByStmp(cellvalue);
}
function timeStampUnFormat(cellvalue, options, cell){
	if(cellvalue=='')return 0;
	var d=new Date(cellvalue);
	var stamp=d.getTime();
	if(stamp==NaN)stamp=0;
	return stamp/1000;
}
/**
 * 加载弹出层表格
 * @param elem
 * @param extConfig
 * @param url
 * @returns {Object} dialog
 */
function gridDialog(elem,extConfig,url){
	var obj;
	if($("#"+elem).length<1){
		$("#load_dialog_container").append("<div id='"+elem+"'></div>");
	}
	var obj=$("#"+elem);
	if(!isEmpty(url)){
		obj.load(url+"?layout=load");
	}
	var defaultConfig={
			height:400,
			width:600,
			autoOpen:false,
			modal:true,
			resizable:false,
			closeOnEscape:true
	};
	$.extend(defaultConfig,extConfig);
	var dialog=obj.dialog(defaultConfig);
	
	return dialog;
}