$(function(){
	$("#username").focus();
	$("#footer").hide();
	$("#login_btn").button().click(function(){
		$("#login_status").html("").removeClass("red");
		var username=trim($("#username").val());
		if(username.length==0){
			showTip($("#username"),"请输入正确的用户名！");
			$("#username").focus();
			return false;
		}
		hideTip($("#username"));
		var password=trim($("#password").val());
		if(password.length==0){
			showTip($("#password"),"请输入正确的密码！");
			$("#password").focus();
			return false;
		}
		hideTip($("#password"));
		checkVerify(function(){
			$("#login_btn").button("disable");
			$.post(BASE+"/admin/login",{"username":username,"password":password},function(e){
				$("#login_btn").button("enable");
				if(e.status==1){
					$(document).unbind("keydown");
					artTip(e.info,3000,function(){
						location.href=BASE+"/admin/home";
					});
				}else{
					$(".verify_refresh").click();
					$("#login_status").html(e.info).addClass("error");
				}
			},"json");
		});
	});
	$(document).bind("keydown",function(event){
		if (event.keyCode=="13"){
			$("#login_btn").click();
		 }
	});
})