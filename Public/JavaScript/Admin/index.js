/**
 * --------------------------------------------------
 * 管理框架
 * --------------------------------------------------
 */
//tabs全局变量
var tabs;
$(document).ready(function(){
	/* tabs相关 */
	//形成tab标签
	tabs=$("#tabs").tabs();
	//为tabs超出部分加入箭头
	tabs.tabs('paging',{cycle: false, follow: false});
	//为每个tab增加关闭按钮功能
	tabs.tabs({
		add:function(e, ui) {
      		$(ui.tab).parents("li:first")
           	 	.append("<span class=\"ui-tabs-close ui-icon ui-icon-close\" title=\"关闭\"></span>")
            	.find("span.ui-tabs-close")
            	.click(function() {
                	tabs.tabs("remove", $("li", tabs).index($(this).parents("li:first")[0]));
            	});
        	tabs.tabs("select", "#" + ui.panel.id);
    	}
	});
	
	//关闭AJAX相应的缓存
	$.ajaxSetup ({
	    cache: false 
	});
	
   	//菜单配置
	$("#menu").jqGrid({
		treeGrid: true,
        treeGridModel:"adjacency",
        ExpandColumn:"menu",
        ExpandColClick:true,
        url:XML+$("#menu").attr("src"),	//请求获得的数据的路径
        datatype: "XML",	//数据类型
        colNames: ["节点编号","主菜单","节点模版","节点说明","需要权限"],
        colModel: [{name: "id",index: "id",hidden: true,sortable: false},
            {name: "menu",index: "menu",hidden: false,sortable: false},
         	{name: "action",index: "action",hidden: true},
        	{name: "tip",index: "tip",hidden: true,sortable: false},
        	{name: "power",index: "power",hidden: true,sortable: false}
	    ],
	    pager:false,
		width:200,
	    viewrecords:true,
	    //caption:"&nbsp;",
	    shrikToFit:false,
	    onSelectRow:function(id){ 
	    	//获得选中行
	    	var selectRow=$("#menu").getRowData(id);
			//当存在下面的tab的时候则选中
			if($("#tabs_"+id).size()>0){
				tabs.tabs("select","#tabs_"+id);
			}
			else{
				//添加一个新的tab
				var action=selectRow.action;
				if(!isEmpty(action)){
					openTab(id,selectRow.menu,BASE+"/admin/"+action);
				}
			}
	    },
	    //加载完毕
	    gridComplete:function(){
	    	menuAnchor();
	    	openTabFromAnchor();
	    }
    }).jqGrid('bindKeys');
	
	var timeout;
	$(window).resize(function(){
		if(isDefined(timeout)) clearTimeout(timeout);
		timeout=setTimeout(resizeElements,10);
	})
	resizeElements();
	
	//固定式提示
	$(".absolute_tip_trigger").live("click",function(){
		var tip=$(this).parent().find(".absolute_tip");
		$(this).hide(0);
		tip.show("slide",{direction:"right"},"slow");
	})
	
	$(".absolute_tip_close").live("click",function(){
		var absolute=$(this).parent().parent().find(".absolute_tip_trigger");
		$(this).parent().hide("slide",{direction:"right"},"slow");
		absolute.show(0);
	})
	
	//关闭所有标签页
	$("#tabs_close_all").click(function(){
		artConfirm("您确认关闭所有标签页吗？",function(){
			$("#ui_tabs .ui-icon-close").each(function(i,e){
				if($(e).attr("id")!="tabs_close_all"){
					$(e).click();
				}
			});
		})
	})
});
/**
 * 使得各元素自适应尺寸
 */
function resizeElements(){
	//左侧菜单容器高度
	var height=$("#content .left_panel:first").height();
	//菜单高度
	$("#menu").setGridHeight(height-25);
	//右侧主界面
	$("#tabs").height(height-7);
	$("#tabs .tabs").height(height-60);
	//右侧主界面中左右结构
	var absoluteWidth=525;
	var leftTableWidth=$(window).width()-absoluteWidth;
	var minTableWidth=$("div[aria-hidden='false'] .main").attr("width");
	if(!isDefined(minTableWidth)) minTableWidth=600;
	leftTableWidth=leftTableWidth>minTableWidth?leftTableWidth:minTableWidth;
	$(".main").width(parseInt(leftTableWidth)+250);
	$(".main_left").width(leftTableWidth);
	$(".main_left .jqgrid_left").setGridWidth(leftTableWidth);
	$(".main_left .jqgrid_left").setGridHeight(height-150);
	//右侧主界面满页表格
	$(".main_full").width(leftTableWidth+230);
	$(".main_full .jqgrid_full").setGridWidth(leftTableWidth+230);
	$(".main_full .jqgrid_full").setGridHeight(height-180);
}