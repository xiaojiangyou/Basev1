/**
 *  ueditor完整配置项
 */

(function () {
	//编辑器根目录
	window.UEDITOR_HOME_URL=ROOT+"/Public/Plugins/Ueditor/";	
    var URL = window.UEDITOR_HOME_URL || getUEBasePath();

    /**
     * 配置项主体
     */
    window.UEDITOR_CONFIG = {
        //为编辑器实例添加一个路径，这个不能被注释
        UEDITOR_HOME_URL : URL

        //图片上传配置区
        ,imageUrl:BASE+"/Admin/Ueditor/upload_file?type=Image"             //图片上传提交地址
        ,imagePath:WEB_ROOT                      //图片修正地址，引用了fixedImagePath,如有特殊需求，可自行配置
        //,imageFieldName:"upfile"                  //图片数据的key,若此处修改，需要在后台对应文件修改对应参数
        //,compressSide:0                           //等比压缩的基准，确定maxImageSideLength参数的参照对象。0为按照最长边，1为按照宽度，2为按照高度
        //,maxImageSideLength:900                   //上传图片最大允许的边长，超过会自动等比缩放,不缩放就设置一个比较大的值，更多设置在image.html中
        ,savePath: [ '/' ]    //图片保存在服务器端的目录， 默认为空， 此时在上传图片时会向服务器请求保存图片的目录列表，
                                                            // 如果用户不希望发送请求， 则可以在这里设置与服务器端能够对应上的目录名称列表
                                                            //比如： savePath: [ 'upload1', 'upload2' ]

        //附件上传配置区
        ,fileUrl:BASE + "/Admin/Ueditor/upload_file?type=File"               //附件上传提交地址
        ,filePath:WEB_ROOT                  //附件修正地址，同imagePath
        //,fileFieldName:"upfile"                    //附件提交的表单名，若此处修改，需要在后台对应文件修改对应参数

        //图片在线管理配置区
        ,imageManagerUrl:BASE + "/Admin/Ueditor/get_image"       //图片在线管理的处理地址
        ,imageManagerPath:WEB_ROOT                                 //图片修正地址，同imagePath

        //视频上传配置区
        ,getMovieUrl:BASE + "/Admin/Ueditor/parse_video"                   //视频数据获取地址
        ,videoUrl:BASE + "/Admin/Ueditor/upload_file?type=Video"               //附件上传提交地址
        ,videoPath:WEB_ROOT                  //附件修正地址，同imagePath
        //,videoFieldName:"upfile"                    //附件提交的表单名，若此处修改，需要在后台对应文件修改对应参数

        //工具栏上的所有的功能按钮和下拉框，可以在new编辑器的实例时选择自己需要的从新定义
        , toolbars:[
            ['fullscreen', 'source', '|', 'undo', 'redo', '|',
                'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', 'pasteplain', '|', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', 'cleardoc', '|',
                'rowspacingtop', 'rowspacingbottom', 'lineheight', '|',
                'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
                'directionalityltr', 'directionalityrtl', 'indent', '|',
                'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|', 'touppercase', 'tolowercase', '|',
                'link', 'unlink', 'anchor', '|', 'imagenone', 'imageleft', 'imageright', 'imagecenter', '|',
                'insertimage', 'emotion', 'scrawl', 'insertvideo', 'music', 'attachment', 'map', 'gmap', 'insertframe','insertcode', 'webapp', 'pagebreak', 'template', 'background', '|',
                'horizontal', 'date', 'time', 'spechars', 'snapscreen', 'wordimage', '|',
                'inserttable', 'deletetable', 'insertparagraphbeforetable', 'insertrow', 'deleterow', 'insertcol', 'deletecol', 'mergecells', 'mergeright', 'mergedown', 'splittocells', 'splittorows', 'splittocols', 'charts', '|',
                'print', 'preview', 'searchreplace', 'help', 'drafts']
        ]
     
        //如果自定义，最好给p标签如下的行高，要不输入中文时，会有跳动感
        ,initialStyle:'p{line-height:1em;font-size:14px;}'//编辑器层级的基数,可以用来改变字体等

        //,autoSyncData:true //自动同步编辑器要提交的数据
        ,emotionLocalization:true //是否开启表情本地化，默认关闭。若要开启请确保emotion文件夹下包含官网提供的images表情文件夹

        ,listiconpath : URL+'themes/ueditor-list/'//自定义标号的路径
    };

    function getUEBasePath ( docUrl, confUrl ) {

        return getBasePath( docUrl || self.document.URL || self.location.href, confUrl || getConfigFilePath() );

    }

    function getConfigFilePath () {

        var configPath = document.getElementsByTagName('script');

        return configPath[ configPath.length -1 ].src;

    }

    function getBasePath ( docUrl, confUrl ) {

        var basePath = confUrl;


        if(/^(\/|\\\\)/.test(confUrl)){

            basePath = /^.+?\w(\/|\\\\)/.exec(docUrl)[0] + confUrl.replace(/^(\/|\\\\)/,'');

        }else if ( !/^[a-z]+:/i.test( confUrl ) ) {

            docUrl = docUrl.split( "#" )[0].split( "?" )[0].replace( /[^\\\/]+$/, '' );

            basePath = docUrl + "" + confUrl;

        }

        return optimizationPath( basePath );

    }

    function optimizationPath ( path ) {

        var protocol = /^[a-z]+:\/\//.exec( path )[ 0 ],
            tmp = null,
            res = [];

        path = path.replace( protocol, "" ).split( "?" )[0].split( "#" )[0];

        path = path.replace( /\\/g, '/').split( /\// );

        path[ path.length - 1 ] = "";

        while ( path.length ) {

            if ( ( tmp = path.shift() ) === ".." ) {
                res.pop();
            } else if ( tmp !== "." ) {
                res.push( tmp );
            }

        }

        return protocol + res.join( "/" );

    }

    window.UE = {
        getUEBasePath: getUEBasePath
    };

})();
