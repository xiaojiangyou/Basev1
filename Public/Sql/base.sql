SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `app_admin`
-- ----------------------------
DROP TABLE IF EXISTS `app_admin`;
CREATE TABLE `app_admin` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT COMMENT '管理员编号',
  `username` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `name` varchar(20) NOT NULL COMMENT '真实姓名',
  `group_id` tinyint(4) DEFAULT NULL COMMENT '管理组编号',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='管理员信息表';

-- ----------------------------
-- Records of app_admin
-- ----------------------------
INSERT INTO `app_admin` VALUES ('1', 'admin', '21232f297a57a5a743894a0e4a801fc3', '开发者', '1');

-- ----------------------------
-- Table structure for `app_group`
-- ----------------------------
DROP TABLE IF EXISTS `app_group`;
CREATE TABLE `app_group` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT COMMENT '管理组编号',
  `name` varchar(10) NOT NULL COMMENT '管理组名称',
  `level` smallint(6) NOT NULL COMMENT '管理组等级',
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='管理组编号';

-- ----------------------------
-- Records of app_group
-- ----------------------------
INSERT INTO `app_group` VALUES ('1', '系统管理员','0');

-- ----------------------------
-- Table structure for `app_power`
-- ----------------------------
DROP TABLE IF EXISTS `app_power`;
CREATE TABLE `app_power` (
  `group_id` tinyint(4) NOT NULL COMMENT '管理组编号',
  `power_id` varchar(10) NOT NULL COMMENT '权限编号',
  `has_power` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否有权限',
  PRIMARY KEY (`group_id`,`power_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单权限表';

-- ----------------------------
-- Table structure for `app_operation`
-- ----------------------------
DROP TABLE IF EXISTS `app_operation`;
CREATE TABLE `app_operation` (
  `group_id` int(10) NOT NULL COMMENT '管理组编号',
  `edit` varchar(100) NOT NULL COMMENT '编辑权限',
  `del` varchar(100) NOT NULL COMMENT '删除权限',
  `add` varchar(100) DEFAULT NULL COMMENT '新增权限',
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '操作权限表';

-- ----------------------------
-- Table structure for `app_img`
-- ----------------------------
DROP TABLE IF EXISTS `app_img`;
CREATE TABLE `app_img` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aid` int(10) DEFAULT '0' COMMENT '相册编号',
  `uid` int(10) DEFAULT '0' COMMENT '上传人',
  `name` varchar(50) NOT NULL COMMENT '图片标题',
  `path` varchar(200) NOT NULL COMMENT '地址',
  `filename` varchar(100) NOT NULL COMMENT '文件名',
  `size` int(10) NOT NULL COMMENT '图片大小',
  `ext` varchar(5) NOT NULL COMMENT '扩展名',
  `description` varchar(500) DEFAULT NULL COMMENT '描述',
  `dateline` int(10) NOT NULL COMMENT '时间戳',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='图片表';


-- ----------------------------
-- Table structure for `app_file`
-- ----------------------------
DROP TABLE IF EXISTS `app_file`;
CREATE TABLE `app_file` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) DEFAULT '0' COMMENT '上传人',
  `name` varchar(50) DEFAULT NULL COMMENT '图片标题',
  `path` varchar(200) NOT NULL COMMENT '地址',
  `filename` varchar(100) NOT NULL COMMENT '文件名',
  `size` int(10) NOT NULL COMMENT '文件大小',
  `ext` varchar(5) NOT NULL COMMENT '扩展名',
  `description` varchar(500) DEFAULT NULL COMMENT '描述',
  `dateline` int(10) NOT NULL COMMENT '时间戳',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='文件表';


-- ----------------------------
-- Table structure for `app_module`
-- ----------------------------
DROP TABLE IF EXISTS `app_module`;
CREATE TABLE `app_module` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL COMMENT '版块名称',
  `description` varchar(500) DEFAULT NULL,
  `seen` tinyint(4) NOT NULL DEFAULT '0',
  `position` tinyint(4) DEFAULT '0' COMMENT '版块位置',
  `url` varchar(500) DEFAULT NULL,
  `img` varchar(500) DEFAULT NULL,
  `allow_comment` tinyint(4) DEFAULT '0' COMMENT '是否允许评论',
  `contents` tinyint(4) DEFAULT '1',
  `push_position` smallint(6) DEFAULT '0',
  `push_count` smallint(6) DEFAULT '8' COMMENT '首页显示条数',
  `module_level` smallint(6) DEFAULT '1' COMMENT '层次',
  `module_pid` smallint(6) DEFAULT '0' COMMENT '父节点',
  `module_leaf` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='模块表';
-- ----------------------------
-- Records of app_module
-- ----------------------------
INSERT INTO `app_module` VALUES ('1', '默认', '', '0', '0', '', '', '0', 1, '0', '0', '1', '0', '1');


-- ----------------------------
-- Table structure for `app_post`
-- ----------------------------
DROP TABLE IF EXISTS `app_post`;
CREATE TABLE `app_post` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `mid` smallint(6) NOT NULL COMMENT '模块编号',
  `uid` int(11) NOT NULL COMMENT '发布人',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  `dateline` int(10) NOT NULL COMMENT '时间戳',
  `comments` smallint(6) DEFAULT '0' COMMENT '评论数',
  `views` int(10) DEFAULT '0' COMMENT '浏览数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `app_post`
ADD COLUMN `img`  varchar(255) NULL AFTER `content`,
ADD COLUMN `introduction`  varchar(255) NULL AFTER `img`;

-- ----------------------------
-- Table structure for `app_word`
-- ----------------------------
DROP TABLE IF EXISTS `app_word`;
CREATE TABLE `app_word` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT '显示名称',
  `lang` varchar(255) NOT NULL COMMENT '语言包代码',
  `value` varchar(500) NOT NULL COMMENT '语言包内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='语言表';

-- ----------------------------
-- Table structure for `app_banner`
-- ----------------------------
DROP TABLE IF EXISTS `app_banner`;
CREATE TABLE `app_banner` (
  `id` smallint(10) NOT NULL AUTO_INCREMENT,
  `position` tinyint(4) DEFAULT '0' COMMENT '显示位置',
  `link` varchar(200) DEFAULT NULL COMMENT '链接地址',
  `url` varchar(200) NOT NULL COMMENT '图片源地址',
  `title` varchar(100) DEFAULT NULL COMMENT '图片名称',
  `show` tinyint(4) DEFAULT '1' COMMENT '是否显示',
  `belong` varchar(20) COMMENT '归属页面',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='幻灯表';

-- ----------------------------
-- Table structure for `app_album`
-- ----------------------------
DROP TABLE IF EXISTS `app_album`;
CREATE TABLE `app_album` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '相册编号',
  `name` varchar(50) NOT NULL COMMENT '相册名称',
  `uid` int(10) NOT NULL COMMENT '上传者',
  `description` varchar(500) DEFAULT NULL,
  `position` smallint(6) DEFAULT '0' COMMENT '位置',
  `official` tinyint(4) DEFAULT '0' COMMENT '是否官方',
  `common` tinyint(4) DEFAULT '0' COMMENT '公用相册',
  `seen` tinyint(4) DEFAULT '0' COMMENT '是否可见',
  `counts` tinyint(4) DEFAULT '0' COMMENT '图片数',
  `dateline` int(10) NOT NULL COMMENT '时间戳',
  `cover` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_album
-- ----------------------------
INSERT INTO `app_album` VALUES ('1', '默认相册', '0', '', '0', '0', '1', '1', '0', '1388505600', '0');
UPDATE `app_album` SET id=0 WHERE id=1;

-- ----------------------------
-- Table structure for `app_link`
-- ----------------------------
DROP TABLE IF EXISTS `app_link`;
CREATE TABLE `app_link` (
  `id` smallint(10) NOT NULL AUTO_INCREMENT,
  `position` tinyint(4) DEFAULT '0' COMMENT '显示位置',
  `link` varchar(200) NOT NULL COMMENT '链接地址',
  `url` varchar(200) DEFAULT NULL COMMENT '图片源地址',
  `title` varchar(100) NOT NULL COMMENT '图片名称',
  `show` tinyint(4) DEFAULT '1' COMMENT '是否显示',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='链接表';

ALTER TABLE `app_post`
ADD COLUMN `top`  tinyint NULL DEFAULT 0 AFTER `dateline`;