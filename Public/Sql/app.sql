
-- ----------------------------
-- Table structure for `app_module`
-- ----------------------------
DROP TABLE IF EXISTS `app_module`;
CREATE TABLE `app_module` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL COMMENT '版块名称',
  `description` varchar(500) DEFAULT NULL,
  `seen` tinyint(4) NOT NULL DEFAULT '0',
  `position` tinyint(4) DEFAULT '0' COMMENT '版块位置',
  `url` varchar(500) DEFAULT NULL,
  `img` varchar(500) DEFAULT NULL,
  `allow_comment` tinyint(4) DEFAULT '0' COMMENT '是否允许评论',
  `contents` tinyint(4) DEFAULT '1',
  `push_position` smallint(6) DEFAULT '0',
  `push_count` smallint(6) DEFAULT '8' COMMENT '首页显示条数',
  `module_level` smallint(6) DEFAULT '1' COMMENT '层次',
  `module_pid` smallint(6) DEFAULT '0' COMMENT '父节点',
  `module_leaf` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8 COMMENT='模块表';

-- ----------------------------
-- Records of app_module
-- ----------------------------
INSERT INTO `app_module` VALUES ('1', '默认', '', '0', '0', '', '', '0', '1', '0', '0', '1', '0', '1');
INSERT INTO `app_module` VALUES ('2', '首页', '', '1', '0', '', '', '0', '0', '0', '0', '1', '0', '1');
INSERT INTO `app_module` VALUES ('3', '学院概况', '', '1', '0', '', '', '0', '0', '0', '0', '1', '0', '0');
INSERT INTO `app_module` VALUES ('4', '师资队伍', '', '1', '0', '', '', '0', '0', '0', '0', '1', '0', '0');
INSERT INTO `app_module` VALUES ('5', '人才培养', '', '1', '0', '', '', '0', '0', '0', '0', '1', '0', '0');
INSERT INTO `app_module` VALUES ('6', '科学研究', '', '1', '0', '', '', '0', '0', '0', '0', '1', '0', '0');
INSERT INTO `app_module` VALUES ('7', '招生工作', '', '1', '0', '', '', '0', '0', '2', '0', '1', '0', '0');
INSERT INTO `app_module` VALUES ('8', '合作交流', '', '1', '0', '', '', '0', '0', '0', '0', '1', '0', '1');
INSERT INTO `app_module` VALUES ('9', '党建工作', '', '1', '0', '', '', '0', '0', '0', '0', '1', '0', '0');
INSERT INTO `app_module` VALUES ('10', '学生园地', '', '1', '0', '', '', '0', '0', '0', '0', '1', '0', '0');
INSERT INTO `app_module` VALUES ('11', '教育科学研究院', '', '1', '0', '', '', '0', '0', '0', '0', '1', '0', '0');
INSERT INTO `app_module` VALUES ('12', '学院介绍', '', '1', '0', '', '', '0', '0', '2', '0', '2', '3', '1');
INSERT INTO `app_module` VALUES ('13', '学院领导', '', '1', '0', '', '', '0', '0', '0', '0', '2', '3', '1');
INSERT INTO `app_module` VALUES ('14', '组织机构', '', '1', '0', '', '', '0', '0', '2', '0', '2', '3', '1');
INSERT INTO `app_module` VALUES ('15', '校园景观', '', '1', '0', '', '', '0', '0', '0', '0', '2', '3', '1');
INSERT INTO `app_module` VALUES ('16', '师资概况', '', '1', '0', '', '', '0', '0', '2', '0', '2', '4', '1');
INSERT INTO `app_module` VALUES ('17', '教职工名单', '', '1', '0', '', '', '0', '0', '0', '0', '2', '4', '1');
INSERT INTO `app_module` VALUES ('18', '导师简介', '', '1', '0', '', '', '0', '0', '0', '0', '2', '4', '1');
INSERT INTO `app_module` VALUES ('19', '博士点', '', '1', '0', '', '', '0', '0', '0', '0', '2', '5', '1');
INSERT INTO `app_module` VALUES ('20', '硕士点', '', '1', '0', '', '', '0', '0', '0', '0', '2', '5', '1');
INSERT INTO `app_module` VALUES ('21', '本科生', '', '1', '0', '', '', '0', '0', '0', '0', '2', '5', '0');
INSERT INTO `app_module` VALUES ('22', '重点学科', '', '1', '0', '', '', '0', '0', '0', '0', '2', '5', '1');
INSERT INTO `app_module` VALUES ('23', '研究方向', '', '1', '0', '', '', '0', '0', '0', '0', '2', '6', '1');
INSERT INTO `app_module` VALUES ('24', '研究团队', '', '1', '0', '', '', '0', '0', '0', '0', '2', '6', '1');
INSERT INTO `app_module` VALUES ('25', '平台基地', '', '1', '0', '', '', '0', '0', '0', '0', '2', '6', '1');
INSERT INTO `app_module` VALUES ('26', '科研结果', '', '1', '0', '', '', '0', '0', '0', '0', '2', '6', '1');
INSERT INTO `app_module` VALUES ('27', '本科生', '', '1', '0', '', '', '0', '0', '0', '0', '2', '7', '0');
INSERT INTO `app_module` VALUES ('28', '研究生', '', '1', '0', '', '', '0', '0', '0', '0', '2', '7', '1');
INSERT INTO `app_module` VALUES ('29', '组织机构', '', '1', '0', '', '', '0', '0', '0', '0', '2', '9', '1');
INSERT INTO `app_module` VALUES ('30', '规章制度', '', '1', '0', '', '', '0', '0', '0', '0', '2', '9', '1');
INSERT INTO `app_module` VALUES ('31', '理论学习', '', '1', '0', '', '', '0', '0', '0', '0', '2', '9', '1');
INSERT INTO `app_module` VALUES ('32', '党校培养', '', '1', '0', '', '', '0', '0', '0', '0', '2', '9', '1');
INSERT INTO `app_module` VALUES ('33', '支部活动', '', '1', '0', '', '', '0', '0', '0', '0', '2', '9', '1');
INSERT INTO `app_module` VALUES ('34', '入党指南', '', '1', '0', '', '', '0', '0', '0', '0', '2', '9', '1');
INSERT INTO `app_module` VALUES ('35', '学工办', '', '1', '0', '', '', '0', '0', '0', '0', '2', '10', '0');
INSERT INTO `app_module` VALUES ('36', '团委', '', '1', '0', '', '', '0', '0', '0', '0', '2', '10', '0');
INSERT INTO `app_module` VALUES ('37', '活动掠影', '', '1', '0', '', '', '0', '0', '0', '0', '2', '10', '1');
INSERT INTO `app_module` VALUES ('38', '学生风采', '', '1', '0', '', '', '0', '0', '0', '0', '2', '10', '1');
INSERT INTO `app_module` VALUES ('39', '工作职责', '', '1', '0', '', '', '0', '0', '0', '0', '3', '35', '1');
INSERT INTO `app_module` VALUES ('40', '勤工助学', '', '1', '0', '', '', '0', '0', '0', '0', '3', '35', '1');
INSERT INTO `app_module` VALUES ('41', '奖学金', '', '1', '0', '', '', '0', '0', '0', '0', '3', '35', '1');
INSERT INTO `app_module` VALUES ('42', '校园文化', '', '1', '0', '', '', '0', '0', '0', '0', '3', '36', '1');
INSERT INTO `app_module` VALUES ('43', '社会实践', '', '1', '0', '', '', '0', '0', '0', '0', '3', '36', '1');
INSERT INTO `app_module` VALUES ('44', '学生科研', '', '1', '0', '', '', '0', '0', '0', '0', '3', '36', '1');
INSERT INTO `app_module` VALUES ('45', '团日活动', '', '1', '0', '', '', '0', '0', '0', '0', '3', '36', '1');
INSERT INTO `app_module` VALUES ('46', '学生会', '', '1', '0', '', '', '0', '0', '0', '0', '3', '36', '1');
INSERT INTO `app_module` VALUES ('47', '学生科协', '', '1', '0', '', '', '0', '0', '0', '0', '3', '36', '1');
INSERT INTO `app_module` VALUES ('48', '社团联合会', '', '1', '0', '', '', '0', '0', '0', '0', '3', '36', '1');
INSERT INTO `app_module` VALUES ('49', '招生咨询', '', '1', '0', '', '', '0', '0', '0', '0', '3', '27', '1');
INSERT INTO `app_module` VALUES ('50', '招生政策', '', '1', '0', '', '', '0', '0', '0', '0', '3', '27', '1');
INSERT INTO `app_module` VALUES ('51', '招生问答', '', '1', '0', '', '', '0', '0', '0', '0', '3', '27', '1');
INSERT INTO `app_module` VALUES ('52', '联系方式', '', '1', '0', '', '', '0', '0', '0', '0', '3', '27', '1');
INSERT INTO `app_module` VALUES ('53', '专业介绍', '', '1', '0', '', '', '0', '0', '0', '0', '3', '21', '1');
INSERT INTO `app_module` VALUES ('54', '培养方案', '', '1', '0', '', '', '0', '0', '0', '0', '3', '21', '1');
INSERT INTO `app_module` VALUES ('55', '日常教务', '', '1', '0', '', '', '0', '0', '0', '0', '3', '21', '1');
INSERT INTO `app_module` VALUES ('56', '导师制', '', '1', '0', '', '', '0', '0', '0', '0', '3', '21', '1');
INSERT INTO `app_module` VALUES ('57', '学院新闻', '', '0', '1', '', '', '0', '1', '1', '10', '2', '64', '1');
INSERT INTO `app_module` VALUES ('58', '学工动态', '', '0', '4', '', '', '0', '1', '1', '10', '2', '64', '1');
INSERT INTO `app_module` VALUES ('60', '就业信息', '', '0', '6', '', '', '0', '1', '1', '10', '2', '64', '1');
INSERT INTO `app_module` VALUES ('61', '下载专区', '', '0', '0', '', '', '0', '1', '2', '0', '2', '64', '1');
INSERT INTO `app_module` VALUES ('62', '通知公告', '', '0', '0', '', '', '0', '1', '3', '10', '2', '64', '1');
INSERT INTO `app_module` VALUES ('63', '学术活动', '', '0', '2', '', '', '0', '1', '1', '10', '2', '64', '1');
INSERT INTO `app_module` VALUES ('64', '学院资讯', '', '0', '2', '', '', '0', '1', '0', '0', '1', '0', '0');
INSERT INTO `app_module` VALUES ('67', '继续教育', '', '1', '0', '', '', '0', '0', '0', '0', '2', '7', '1');
INSERT INTO `app_module` VALUES ('68', '研究院介绍', '', '1', '0', '', '', '0', '0', '0', '0', '2', '11', '1');
INSERT INTO `app_module` VALUES ('69', '基本职能', '', '1', '0', '', '', '0', '0', '0', '0', '2', '11', '1');
INSERT INTO `app_module` VALUES ('70', '现有学科', '', '1', '0', '', '', '0', '0', '0', '0', '2', '11', '1');
INSERT INTO `app_module` VALUES ('71', '导师名单', '', '1', '0', '', '', '0', '0', '0', '0', '2', '11', '1');
INSERT INTO `app_module` VALUES ('72', '研究所设置', '', '1', '0', '', '', '0', '0', '0', '0', '2', '11', '1');
INSERT INTO `app_module` VALUES ('73', '教育论坛', '', '1', '0', '', '', '0', '0', '0', '0', '2', '11', '1');
INSERT INTO `app_module` VALUES ('74', '特色培训', '', '0', '0', '', '', '0', '0', '2', '0', '1', '0', '0');
INSERT INTO `app_module` VALUES ('75', '培训计划', '', '1', '0', '', '', '0', '0', '0', '0', '2', '74', '1');
INSERT INTO `app_module` VALUES ('76', '培训内容', '', '1', '0', '', '', '0', '0', '0', '0', '2', '74', '1');
INSERT INTO `app_module` VALUES ('77', '培新信息', '', '1', '0', '', '', '0', '1', '0', '0', '2', '74', '1');
INSERT INTO `app_module` VALUES ('78', '教工之家', '', '1', '5', '', '', '0', '1', '1', '0', '2', '64', '1');
INSERT INTO `app_module` VALUES ('79', '党建信息', '', '0', '3', '', '', '0', '1', '1', '10', '2', '64', '1');
INSERT INTO `app_module` VALUES ('80', '心理健康教育', '', '1', '0', '', '', '0', '0', '0', '0', '2', '10', '1');


-- ----------------------------
-- Table structure for `app_link`
-- ----------------------------
DROP TABLE IF EXISTS `app_link`;
CREATE TABLE `app_link` (
  `id` smallint(10) NOT NULL AUTO_INCREMENT,
  `position` tinyint(4) DEFAULT '0' COMMENT '显示位置',
  `link` varchar(200) NOT NULL COMMENT '链接地址',
  `url` varchar(200) DEFAULT NULL COMMENT '图片源地址',
  `title` varchar(100) NOT NULL COMMENT '图片名称',
  `show` tinyint(4) DEFAULT '1' COMMENT '是否显示',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='链接表';
-- ----------------------------
-- Records of app_link
-- ----------------------------
INSERT INTO `app_link` VALUES ('1', '0', 'http://www.suda.edu.cn/', '', '苏州大学', '1');
INSERT INTO `app_link` VALUES ('2', '0', 'http://xlzx.suda.edu.cn/Index/index.aspx', '', '苏南地区大学生心理健康教育研究中心', '1');
INSERT INTO `app_link` VALUES ('3', '0', 'http://xjzx.suda.edu.cn', '', '苏州大学心理与教师教育实验教学中心', '1');
INSERT INTO `app_link` VALUES ('4', '0', 'http://www.ec.js.edu.cn/', '', '江苏教育', '1');
INSERT INTO `app_link` VALUES ('5', '0', 'http://library.suda.edu.cn/', '', '苏州大学图书馆', '1');
INSERT INTO `app_link` VALUES ('6', '0', 'http://journal.psych.ac.cn/', '', '中国心理学会', '1');


-- ----------------------------
-- Table structure for `app_word`
-- ----------------------------
DROP TABLE IF EXISTS `app_word`;
CREATE TABLE `app_word` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT '显示名称',
  `lang` varchar(255) NOT NULL COMMENT '语言包代码',
  `value` varchar(500) NOT NULL COMMENT '语言包内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='语言表';

-- ----------------------------
-- Records of app_word
-- ----------------------------
INSERT INTO `app_word` VALUES ('1', '网站名称', 'SITE_NAME', '苏州大学教育学院');
INSERT INTO `app_word` VALUES ('2', '关键字', 'SITE_KEYWORD', '教育学院,苏州大学');
INSERT INTO `app_word` VALUES ('3', '网站描述', 'SITE_DESCRIPTION', '');
INSERT INTO `app_word` VALUES ('4', '开启首页弹出层', 'AD_OPEN', '1');
INSERT INTO `app_word` VALUES ('5', '弹出层标题', 'AD_TITLE', '弹出层标题');
INSERT INTO `app_word` VALUES ('6', '弹出层链接', 'AD_LINK', '#');
INSERT INTO `app_word` VALUES ('7', '弹出层内容', 'AD_CONTENT', '弹出层内容');